using UnityEngine;

namespace Debug
{
    public class ResumeButton : MonoBehaviour
    {
        [SerializeField] private GameObject settingsPanel;

        public void ResumeGame()
        {
            Time.timeScale = 1f; 
            settingsPanel.SetActive(false);
        }
    }
}
