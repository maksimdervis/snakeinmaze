using UnityEngine;
using UnityEngine.SceneManagement;

namespace Debug
{
    public class RestartButton : MonoBehaviour
    {
        public void RestartGame()
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
