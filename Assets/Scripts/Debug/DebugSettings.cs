﻿using UnityEngine;

namespace Debug
{
    public class DebugSettings
    {
        public bool ShowFrame { get; set; } = true;
    }
}