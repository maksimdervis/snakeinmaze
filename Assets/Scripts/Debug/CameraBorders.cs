﻿using System;
using System.Numerics;
using SnakeCamera;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Vector2 = UnityEngine.Vector2;
using Vector4 = UnityEngine.Vector4;

namespace Debug
{
    public class CameraBorders : MonoBehaviour
    {
        [SerializeField] private Texture transparentBorder;
        [SerializeField] private Camera mainCamera;
        [SerializeField] private FollowingCamera followingCamera;
        
        private DebugSettings _debugSettings;

        private int InnerAreaWidthUnits => (int) Math.Floor((decimal) followingCamera.AreaWidthUnits);
        private int InnerAreaHeightUnits => (int) Math.Floor((decimal) followingCamera.AreaHeightUnits - 1);

        [Inject]
        public void Construct(DebugSettings debugSettings) => 
            _debugSettings = debugSettings;

        private void OnGUI() => 
            DrawInnerAreaBorders();

        private void DrawInnerAreaBorders()
        {
            if (_debugSettings.ShowFrame == false)
                return;

            Color previousColor = GUI.color;
            GUI.color = new Color(1f, 1f, 1f, 0.4f);
            Vector2 innerBorderLeftTop = new Vector2(GetLeftBorder(), GetTopBorder());
            float rectWidth = GetAreaLengthPixels(InnerAreaWidthUnits);
            float rectHeight = GetAreaLengthPixels(InnerAreaHeightUnits);
            float offsetX = followingCamera.AreaWidthUnits - InnerAreaWidthUnits;
            float offsetY = followingCamera.AreaHeightUnits - InnerAreaHeightUnits - 1;
            Rect innerBorderRect = new Rect(innerBorderLeftTop.x + offsetX, innerBorderLeftTop.y + offsetY + GetAreaLengthPixels(0.5f), 
            rectWidth, 
            rectHeight);
            GUI.DrawTexture(innerBorderRect, transparentBorder, ScaleMode.StretchToFill);
            GUI.color = previousColor;
        }

        private float GetLeftBorder()
        {
            float screenCenter = Screen.width / 2f;
            return screenCenter - GetAreaLengthPixels(InnerAreaWidthUnits) / 2f;
        }

        private float GetTopBorder()
        {
            float screenCenter = Screen.height / 2f;
            return screenCenter - GetAreaLengthPixels(InnerAreaHeightUnits) / 2f;
        }
        private float GetAreaLengthPixels(float lengthUnits)
        {
            return lengthUnits * (Screen.height / (mainCamera.orthographicSize * 2f));
        }
    }
}