using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Debug
{
    public class MouseScroller : MonoBehaviour
    {
        [SerializeField] private Camera camera;
        
        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                camera.transform.position = camera.ScreenToWorldPoint(Input.mousePosition);
            }
        }
    }
}
