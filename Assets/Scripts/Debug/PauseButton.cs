using System;
using UnityEngine;
using UnityEngine.UI;

namespace Debug
{
    public class PauseButton : MonoBehaviour
    {
        [SerializeField] private GameObject settingsPanel;

        public void PauseGame()
        {
            Time.timeScale = 0f;
            settingsPanel.SetActive(true);
        }
    }
}
