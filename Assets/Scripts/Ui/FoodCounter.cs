﻿using System;
using Generation.Spawning.Abstractions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ui
{
    public class FoodCounter : MonoBehaviour
    {
        [SerializeField] private Text text;
        
        [Inject]
        public void Construct(MazeRenderer mazeRenderer) => 
            mazeRenderer.OnLeftFoodNumberChanged(ChangeCounterValue);

        private void ChangeCounterValue(int value) => 
            text.text = $"Еды осталось: {value}";
    }
}