﻿using Constants;
using GamePlay;
using Infrastructure.Ui;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Ui
{
    [RequireComponent(typeof(IChosenLevelUiInformation))]
    public class MainMenuUI : MonoBehaviour
    {
        [SerializeField] private Button playButton;
        [SerializeField] private Button playFromFirstLevelButton; 
        private IChosenLevelUiInformation chosenLevelUIInformation;
        private ZenjectSceneLoader _sceneLoader;

        [Inject]
        public void Construct(ZenjectSceneLoader sceneLoader)
        {
            _sceneLoader = sceneLoader;
            playButton.onClick.AddListener(StartLevel);
            playFromFirstLevelButton.onClick.AddListener(StartFromFirstLevel);
            TempSnakeSettings.Pass = false;
            chosenLevelUIInformation = GetComponent<IChosenLevelUiInformation>();
        }
        
        private void StartLevel()
        {
            if (chosenLevelUIInformation.TryAskLevel(out int level) == false)
                return;
            _sceneLoader.LoadSceneAsync("Game", LoadSceneMode.Single, container => container.BindInstance(level + MazeConstants.AddLevel).WithId("Level"));
        }

        private void StartFromFirstLevel() => 
            _sceneLoader.LoadSceneAsync("Game", LoadSceneMode.Single, container => container.BindInstance(1 + MazeConstants.AddLevel).WithId("Level"));
    }
}