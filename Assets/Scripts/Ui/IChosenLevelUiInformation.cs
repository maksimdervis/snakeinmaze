﻿namespace Infrastructure.Ui
{
    public interface IChosenLevelUiInformation
    {
        bool TryAskLevel(out int level);
    }
}