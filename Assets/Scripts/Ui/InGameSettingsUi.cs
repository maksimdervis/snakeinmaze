﻿using System;
using Debug;
using GamePlay;
using Infrastructure.Services.Input;
using SimpleInputNamespace;
using SnakeCamera;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Zenject;

namespace Infrastructure.Ui
{
    public class InGameSettingsUi : MonoBehaviour
    {
        [SerializeField] private Button resumeButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private InputField speedInput;
        [SerializeField] private InputField framesRatioInput;
        [SerializeField] private InputField joystickRadiusInput;
        [SerializeField] private Toggle godModeToggle;
        [SerializeField] private Toggle joystickInputToggle;
        [SerializeField] private Toggle showFrameToggle;
        [SerializeField] private Button set3X3ScaleButton;
        [SerializeField] private Button set4X4ScaleButton;
        [SerializeField] private Button set5X5ScaleButton;
        
        private SnakeSettings _snakeSettings;
        private DebugSettings _debugSettings;
        private InputSettings _inputSettings;
        private CameraConfiguration _cameraConfiguration;

        private ZenjectSceneLoader _sceneLoader;

        [Inject]
        public void Construct(ZenjectSceneLoader sceneLoader, SnakeSettings snakeSettings, DebugSettings debugSettings, 
                            InputSettings inputSettings, CameraConfiguration cameraConfiguration)
        {
            _sceneLoader = sceneLoader;
            _snakeSettings = snakeSettings;
            _debugSettings = debugSettings;
            _inputSettings = inputSettings;
            _cameraConfiguration = cameraConfiguration;
            SetUpListeners();
        }

        private void SetUpListeners()
        {
            resumeButton.onClick.AddListener(ResumeGame);
            restartButton.onClick.AddListener(RestartGame);
            speedInput.onEndEdit.AddListener(ChangeSnakeSpeed);
            framesRatioInput.onEndEdit.AddListener(ChangeFramesRatio);
            godModeToggle.onValueChanged.AddListener(SwitchGodMode);
            joystickInputToggle.onValueChanged.AddListener(ChangeInputSystem);
            showFrameToggle.onValueChanged.AddListener(ShowHideFrame);
            joystickRadiusInput.onValueChanged.AddListener(ChangeJoystickRadius);
            set3X3ScaleButton.onClick.AddListener(Set3X3Scale);
            set4X4ScaleButton.onClick.AddListener(Set4X4Scale);
            set5X5ScaleButton.onClick.AddListener(Set5X5Scale);
        }

        private void Set5X5Scale() => 
            _cameraConfiguration.Set5X5Scale();

        private void Set4X4Scale() => 
            _cameraConfiguration.Set4X4Scale();

        private void Set3X3Scale() => 
            _cameraConfiguration.Set3X3Scale();

        private void ShowHideFrame(bool isVisible) => 
            _debugSettings.ShowFrame = isVisible;

        private void ChangeInputSystem(bool joystickInputSystemEnabled)
        {
            if (joystickInputSystemEnabled)
                _inputSettings.SwitchOnJoystickInputSystem();
            else
                _inputSettings.SwitchOnSwipeInputSystem();
        }

        private void SwitchGodMode(bool isGodModeEnabled) => 
            _snakeSettings.IsGod = isGodModeEnabled;

        private void ChangeJoystickRadius(string radiusStr)
        {
            if (float.TryParse(radiusStr, out float radius) == false)
                return;

            _inputSettings.JoystickMovementAreaRadius = radius;
        }

        private void ChangeFramesRatio(string framesRatioStr)
        {
            if (float.TryParse(framesRatioStr, out float framesRatio) == false)
                return;

            TempSnakeSettings.Pass = true; 
            try
            {
                TempSnakeSettings.ratio = framesRatio;
                _snakeSettings.FramesRatio = framesRatio; 
            }
            catch (ArgumentOutOfRangeException e)
            {
                UnityEngine.Debug.LogWarning(e.Message);
            }
        }

        private void ChangeSnakeSpeed(string speedStr)
        {
            if (float.TryParse(speedStr, out float speed) == false)
                return;

            TempSnakeSettings.Pass = true; 
            try
            {
                TempSnakeSettings.SnakeSpeed = Math.Max(speed, float.Epsilon);
                _snakeSettings.TimeBeforeMiddleFrame = 1 / Math.Max(speed, float.Epsilon);
            }
            catch (ArgumentOutOfRangeException e)
            {
                UnityEngine.Debug.LogWarning(e.Message);
            }
        }

        private void RestartGame()
        {
            TempSnakeSettings.Pass = false;
            _sceneLoader.LoadScene("Initial");
        }

        private void ResumeGame()
        {
            Time.timeScale = 1f; 
            gameObject.SetActive(false);
        }
    }
}