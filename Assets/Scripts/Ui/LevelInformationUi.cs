﻿using Constants;
using Generation.MazeHolding.Abstractions;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Ui
{
    public class LevelInformationUi : MonoBehaviour
    {
        [SerializeField] private Text levelText;
        
        [Inject]
        public void Inject(IMazeData mazeData) => 
            levelText.text = $"Уровень: {mazeData.Level - MazeConstants.AddLevel}";
    }
}