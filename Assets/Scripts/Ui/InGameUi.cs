﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Infrastructure.Ui
{
    public class InGameUi : MonoBehaviour
    {
        [SerializeField] private GameObject settingsPanel; 
        [SerializeField] private Button settingsButton;

        [Inject]
        public void Construct() => 
            settingsButton.onClick.AddListener(OpenSettings);

        private void OpenSettings()
        {
            Time.timeScale = 0f;
            settingsPanel.SetActive(true);
        }
    }
}