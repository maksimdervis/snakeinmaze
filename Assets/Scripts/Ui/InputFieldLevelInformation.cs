﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Infrastructure.Ui
{
    public class InputFieldLevelInformation : MonoBehaviour, IChosenLevelUiInformation
    {
        [SerializeField] private InputField inputField;

        public bool TryAskLevel(out int level) =>
            Int32.TryParse(inputField.text, out level);
    }
}