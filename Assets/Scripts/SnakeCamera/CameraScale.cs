﻿using System;
using UnityEngine;
using Zenject;

namespace SnakeCamera
{
    public class CameraScale : MonoBehaviour
    {
        public float WidthUnits { get; private set; }
        public float HeightUnits { get; private set; }
        public float DefaultOrthographicSize => GetOrthographicSize(CameraConfiguration.MinUnits3X3);

        private int _minUnits;

        private Camera _camera;
        private CameraConfiguration _cameraConfiguration;
        private Vector2Int _previousScreenSize;
        private Action _onCameraScaleChanged;


        [Inject]
        public void Construct(Camera cam, CameraConfiguration cameraConfiguration)
        {
            _camera = cam;
            _previousScreenSize = new Vector2Int(Screen.width, Screen.height);
            _cameraConfiguration = cameraConfiguration;
            ResetCameraScale();
            cameraConfiguration.OnCameraConfigurationChanged(ResetCameraScale);
        }

        private void ResetCameraScale()
        {
            _minUnits = _cameraConfiguration.MinUnitsInArea + 2 * _cameraConfiguration.AreaOffset;
            SetupCameraSize();
            _onCameraScaleChanged?.Invoke();
        }

        public void OnResolutionChanged(Action action) => 
            _onCameraScaleChanged += action;

        private float GetOrthographicSize(int minUnits)
        {
            if (Screen.width > Screen.height)
            {
                if (PossibleToAlignToHeight(minUnits))
                    return GetOrthographicSizeFromHeightUnits(minUnits);
                
                return GetOrthographicSizeFromWidthInUnits(minUnits);
            }

            if (IsPossibleToAlignToWidth(minUnits))
                return GetOrthographicSizeFromWidthInUnits(minUnits);
                
            return GetOrthographicSizeFromHeightUnits(minUnits);
        }
        
        private void Update() => 
            ProcessResolutionChanges();

        private void ProcessResolutionChanges()
        {
            var currentScreenSize = new Vector2Int(Screen.width, Screen.height);
            if (_previousScreenSize == currentScreenSize) 
                return;
            
            SetupCameraSize();
            _previousScreenSize = currentScreenSize;
            _onCameraScaleChanged.Invoke();
        }

        private void SetupCameraSize()
        {
            if (Screen.width > Screen.height)
            {
                if (PossibleToAlignToHeight(_minUnits))
                    SetHeightAlignment();
                else
                    SetWidthAlignment();
            }
            else
            {
                if (IsPossibleToAlignToWidth(_minUnits))
                    SetWidthAlignment();
                else
                    SetHeightAlignment();
            }
        }

        private void SetHeightAlignment()
        {
            AlignToHeight();
            HeightUnits = _minUnits;
            WidthUnits = GetWidthInUnits(_camera.orthographicSize);
        }

        private void SetWidthAlignment()
        {
            AlignToWidth();
            WidthUnits = _minUnits;
            HeightUnits = GetHeightInUnits(_camera.orthographicSize);
        }

        private bool IsPossibleToAlignToWidth(int minUnits)
        {
            float orthographicSize = GetOrthographicSizeFromWidthInUnits(minUnits);
            return minUnits <= GetHeightInUnits(orthographicSize);
        }

        private bool PossibleToAlignToHeight(int minUnits)
        {
            float orthographicSize = GetOrthographicSizeFromHeightUnits(minUnits);
            return minUnits <= GetWidthInUnits(orthographicSize);
        }

        private void AlignToWidth() => 
            _camera.orthographicSize = GetOrthographicSizeFromWidthInUnits(_minUnits);

        private void AlignToHeight() => 
            _camera.orthographicSize = GetOrthographicSizeFromHeightUnits(_minUnits);

        private float GetOrthographicSizeFromHeightUnits(int minUnits) => 
            minUnits / 2f;

        private float GetOrthographicSizeFromWidthInUnits(int minUnits) => 
            minUnits * (Screen.height * 1f / Screen.width) * 0.5f;

        private static float GetHeightInUnits(float orthographicSize) => 
            orthographicSize * 2f;

        private static float GetWidthInUnits(float orthographicSize) => 
            (orthographicSize * Screen.width * 2f) / (Screen.height * 1f);
    }
}
