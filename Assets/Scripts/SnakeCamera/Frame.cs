﻿using System.Collections.Generic;
using Generation.Cells.Abstractions;
using UnityEngine;
using Zenject;

namespace SnakeCamera
{
    public class Frame : MonoBehaviour
    {
        [SerializeField] private GameObject frameLeftTop;
        [SerializeField] private GameObject frameTop; 
        [SerializeField] private GameObject blackBar; 
        
        private CameraScale _cameraScale;
        private Camera _cam;

        private List<GameObject> frameCells;

        [Inject]
        public void Construct(Camera cam, CameraScale cameraScale, CameraConfiguration cameraConfiguration)
        {
            _cam = cam; 
            _cameraScale = cameraScale;
            cameraScale.OnResolutionChanged(SetupFrame);
            cameraConfiguration.OnCameraConfigurationChanged(SetupFrame);
            SetupFrame();
        }

        private void SetupFrame()
        {
            if (frameCells != null)
                foreach (GameObject cell in frameCells)
                    Destroy(cell);

            Transform cameraTransform = _cam.transform;
            float orthographicSize = _cam.orthographicSize;
            Vector3 cameraPosition = cameraTransform.position;

            frameCells = new List<GameObject>();
            Vector2 screenHalf = new Vector2(_cameraScale.WidthUnits, _cameraScale.HeightUnits) / 2;
            Vector2 cameraLeftBottomPos = (Vector2) cameraPosition - screenHalf + Vector2.one / 2;
            Vector2 cameraRightTopPos = (Vector2) cameraPosition + screenHalf - Vector2.one / 2;

            float blackBarOffset = orthographicSize / _cameraScale.DefaultOrthographicSize / 2 + 0.5f;
            Vector2 frameRightTopPos = cameraRightTopPos + Vector2.down * blackBarOffset;
            InstantiateCorners(cameraLeftBottomPos, cameraRightTopPos, blackBarOffset);

            for (float x= cameraLeftBottomPos.x; x <= frameRightTopPos.x; x++) 
                InstantiateBlackBar(x, cameraRightTopPos.y);

            float frameWidth = _cameraScale.WidthUnits - 2;
            float frameHeight = _cameraScale.HeightUnits - 2 - blackBarOffset;
            InstantiateFrameCell(frameTop, cameraRightTopPos.x - 0.5f, frameRightTopPos.y, frameWidth);
            InstantiateFrameCell(frameTop, cameraRightTopPos.x - 0.5f, cameraLeftBottomPos.y, frameWidth).GetComponent<SpriteRenderer>().flipY = true;
            InstantiateFrameCell(frameTop, cameraRightTopPos.x, cameraLeftBottomPos.y + 0.5f, frameHeight).transform.Rotate(Vector3.forward * -90);
            GameObject frameCell = InstantiateFrameCell(frameTop, cameraLeftBottomPos.x, cameraLeftBottomPos.y + 0.5f, frameHeight);
            frameCell.transform.Rotate(Vector3.forward * -90);
            frameCell.GetComponent<SpriteRenderer>().flipY = true;
        }

        private void InstantiateCorners(Vector2 cameraLeftBottomPos, Vector2 cameraRightTopPos, float blackBarOffset)
        {
            InstantiateFrameCell(frameLeftTop, cameraLeftBottomPos.x, cameraRightTopPos.y - blackBarOffset);
            InstantiateFrameCell(frameLeftTop, cameraRightTopPos.x, cameraRightTopPos.y - blackBarOffset).GetComponent<SpriteRenderer>().flipX = true;
            InstantiateFrameCell(frameLeftTop, cameraLeftBottomPos.x, cameraLeftBottomPos.y).GetComponent<SpriteRenderer>().flipY = true;
            GameObject frameRightBottom = InstantiateFrameCell(frameLeftTop, cameraRightTopPos.x, cameraLeftBottomPos.y);
            SpriteRenderer spriteRenderer = frameRightBottom.GetComponent<SpriteRenderer>();
            spriteRenderer.flipX = true;
            spriteRenderer.flipY = true;
        }

        private void InstantiateBlackBar(float x, float y)
        {
            GameObject newBlackBar = InstantiateFrameCell(blackBar, x, y);
            newBlackBar.transform.localScale = Vector3.one * _cam.orthographicSize / _cameraScale.DefaultOrthographicSize;
        }

        private GameObject InstantiateFrameCell(GameObject frameCell, float x, float y, float width = 1)
        {
            Vector2 position = new Vector2(x, y);
            GameObject instance = Instantiate(frameCell, position, Quaternion.identity);
            frameCells.Add(instance);
            instance.transform.SetParent(_cam.transform);
            instance.transform.localScale = new Vector3(width, 1f, 1f); 
            return instance;
        }
    }
}