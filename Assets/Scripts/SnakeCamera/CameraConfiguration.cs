﻿using System;
using Generation.MazeHolding.Abstractions;

namespace SnakeCamera
{
    public class CameraConfiguration
    {
        public CameraConfiguration(IMazeData mazeData)
        {
            switch (mazeData.SectorsPerSide)
            {
                case <= 10:
                    Set3X3Scale();
                    return;
                case <= 20:
                    Set4X4Scale();
                    return;
                default:
                    Set5X5Scale();
                    break;
            }
        }

        public const int MinUnits3X3 = 13;
        public int MinUnitsInArea
        {
            get => _minUnitsInArea;
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException(nameof(value),
                        $"Incorrect minimum sectors amount, got {value}");
                _minUnitsInArea = value;
                _cameraConfigurationChanged?.Invoke();
            }
        }

        public int MaxUnitsInArea
        {
            get => _maxUnitsInArea;
            set
            {
                if (value < _minUnitsInArea)
                    throw new ArgumentOutOfRangeException(nameof(value),
                        $"Max units in area should be greater (or equal to) then min units in area, which is {_minUnitsInArea}");
                _maxUnitsInArea = value;
                _cameraConfigurationChanged?.Invoke();
            }
        }

        public int AreaOffset
        {
            get => _areaOffset;
            set
            {
                if (value < 0)
                    throw new ArgumentOutOfRangeException(nameof(value),
                        $"Incorrect area offset, got negative {value}");
                _areaOffset = value;
                _cameraConfigurationChanged?.Invoke();
            }
        }

        private int _minUnitsInArea;
        private int _maxUnitsInArea;
        private int _areaOffset;
        private Action _cameraConfigurationChanged;

        public void OnCameraConfigurationChanged(Action action) => 
            _cameraConfigurationChanged += action;

        public void Set3X3Scale()
        {
            MinUnitsInArea = 9;
            MaxUnitsInArea = 14;
            AreaOffset = 2;
        }

        public void Set4X4Scale()
        {
            MinUnitsInArea = 11;
            MaxUnitsInArea = 18;
            AreaOffset = 3;
        }

        public void Set5X5Scale()
        {
            MinUnitsInArea = 11;
            MaxUnitsInArea = 22;
            AreaOffset = 5;
        }
    }
}