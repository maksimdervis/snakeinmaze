﻿using System;
using System.Collections;
using System.Numerics;
using Constants;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Enums;
using GamePlay;
using GamePlay.Player.Implementations;
using Generation.MazeHolding.Abstractions;
using Generation.Spawning.Abstractions;
using UnityEngine;
using Zenject;
using Grid = Generation.MazeHolding.Abstractions.Grid;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace SnakeCamera
{
    public class FollowingCamera : MonoBehaviour
    {
        public int AreaWidthUnits
        {
            get
            {
                int normalWidth = (int) _cameraScale.WidthUnits - 2 * _cameraConfiguration.AreaOffset;
                return GetAreaLength(normalWidth);
            }
        }

        public int AreaHeightUnits
        {
            get
            {
                int normalHeight = (int) _cameraScale.HeightUnits - 2 * _cameraConfiguration.AreaOffset;
                return GetAreaLength(normalHeight);
            }
        }

        private int GetAreaLength(int normalLength)
        {
            if (normalLength < _cameraConfiguration.MinUnitsInArea)
                return _cameraConfiguration.MinUnitsInArea;
            if (normalLength > _cameraConfiguration.MaxUnitsInArea)
                return _cameraConfiguration.MaxUnitsInArea;
            return normalLength;
        }


        private float ScreenWidthSectors => GetSectorsAmount(_cameraScale.WidthUnits);
        private float ScreenHeightSectors => GetSectorsAmount(_cameraScale.HeightUnits);
        private float HorizontalDistanceToGridEnd => (_cameraScale.WidthUnits - AreaWidthUnits) / 2f;
        private float VerticalDistanceToGridEnd => (_cameraScale.HeightUnits - AreaHeightUnits) / 2f;

        private Vector2 CameraCenter => (_cameraLeftTop + _cameraRightBottom) / 2;
        private Vector3 NextCameraPosition => (Vector3) CameraCenter + Vector3.forward * transform.position.z;
        private Vector2 AreaLeftTop
        {
            get
            {
                Vector2 value = _cameraLeftTop + new Vector2(HorizontalDistanceToGridEnd, -VerticalDistanceToGridEnd);
                value.x = (float) Math.Ceiling(value.x);
                value.y = (float) Math.Floor(value.y - 1);
                return value;
            }
        }

        private Vector2 AreaRightBottom
        {
            get
            {
                Vector2 value = _cameraRightBottom + new Vector2(-HorizontalDistanceToGridEnd, VerticalDistanceToGridEnd);
                value.x = (float) Math.Floor(value.x);
                value.y = (float) Math.Ceiling(value.y);
                return value;
            }
        }

        private Vector2 _cameraLeftTop;
        private Vector2 _cameraRightBottom;
        
        private IMazeData _mazeData;
        private CameraScale _cameraScale;
        private GridPositioner _gridPositioner;
        private Snake _snake;
        private Grid _grid;
        
        private TweenerCore<Vector3, Vector3, VectorOptions> _previousHorizontalTween;
        private TweenerCore<Vector3, Vector3, VectorOptions> _previousVerticalTween;

        private bool _isMovingVertically;
        private bool _isMovingHorizontally;
        private float _previousHorizontalDirection;
        private float _previousVerticalDirection;

        private TweenCallback moveVertically;
        private TweenCallback moveHorizontally;

        private SnakeSettings _snakeSettings;
        private CameraConfiguration _cameraConfiguration;
        private Camera _cam;

        [Inject]
        public void Construct(IMazeData mazeData, GridPositioner gridPositioner, Snake snake, SnakeSettings snakeSettings, Grid grid, 
                                CameraScale cameraScale, CameraConfiguration cameraConfiguration, Camera cam)
        {
            _mazeData = mazeData;
            _gridPositioner = gridPositioner;
            _snake = snake;
            _snakeSettings = snakeSettings;
            _grid = grid;
            _cam = cam;
            _cameraScale = cameraScale;
            _cameraConfiguration = cameraConfiguration;
        }

        private void Start()
        {
            moveHorizontally = 
                () => _previousHorizontalTween = 
                    MoveNext(GetXScaledAreaOffset, OffsetAreaHorizontally, MoveXToAreaCenter, IsHorizontalMapEnd, ref _isMovingHorizontally, ref _previousHorizontalDirection);
            moveVertically = 
                () => _previousVerticalTween = 
                    MoveNext(GetYScaledAreaOffset, OffsetAreaVertically, MoveYToAreaCenter, IsVerticalMapEnd, ref _isMovingVertically, ref _previousVerticalDirection);
            SetupCamera();
            _cameraScale.OnResolutionChanged(SetupCamera);
        }

        private void SetupCamera()
        {
            _snake.UnsubscribeAll();
            transform.position = (Vector3)(Vector2) _snake.CurrentHeadCell + Vector3.forward * transform.position.z;
            CountCurrentArea();
            StartCoroutine(SubscribeSnake());
        }

        private IEnumerator SubscribeSnake()
        {
            Vector3 nextCamera = (Vector3) (_cameraLeftTop + _cameraRightBottom) / 2 + Vector3.forward * transform.position.z;
            Vector3 gridCenter = GetGridCenter();
            if (ScreenWidthSectors - 2 > _mazeData.SectorsPerSide - 2 && ScreenHeightSectors > _mazeData.SectorsPerSide)
            {
                nextCamera = gridCenter;
            }
            else if (ScreenWidthSectors - 2 > _mazeData.SectorsPerSide)
            {
                nextCamera.x = gridCenter.x;
                _snake.OnSnakeMoved(MoveYOnly);
            }
            else if (ScreenHeightSectors - 2 > _mazeData.SectorsPerSide)
            {
                nextCamera.y = gridCenter.y;
                _snake.OnSnakeMoved(MoveXOnly);
            }
            else
            {
                _snake.OnSnakeMoved(MoveBothDirections);
            }

            //yield return new WaitForSeconds(1f);
            //transform.DOMove(nextCamera, 2f).SetEase(Ease.InOutSine);
            transform.position = nextCamera;
            yield break;
        }

        private void Update()
        {
            Vector3 leftTop = (Vector3) AreaLeftTop + Vector3.forward * (transform.position.z / 2f);
            Vector3 rightBottom = (Vector3) AreaRightBottom + Vector3.forward * (transform.position.z / 2f);
            
            Vector3 leftBottom = leftTop;
            leftBottom.y = rightBottom.y;
            
            Vector3 rightTop = rightBottom;
            rightTop.y = leftTop.y;
            
            UnityEngine.Debug.DrawLine(leftTop, rightTop, Color.green, Time.deltaTime, false);
            UnityEngine.Debug.DrawLine(rightTop, rightBottom, Color.green, Time.deltaTime, false);
            UnityEngine.Debug.DrawLine(rightBottom, leftBottom, Color.green, Time.deltaTime, false);
            UnityEngine.Debug.DrawLine(leftBottom, leftTop, Color.green, Time.deltaTime, false);
        }

        private Vector3 GetGridCenter()
        {
            Vector2 gridRightBottom = _gridPositioner.GetPosition(Vector2Int.one * (_grid.Length - 1));
            Vector2 gridLeftTop = _gridPositioner.GetPosition(Vector2Int.zero);
            Vector3 center = (Vector3) (gridRightBottom + gridLeftTop) / 2;
            return center;
        }
        
        private void MoveBothDirections()
        {
            float areaLeftCopy = _cameraLeftTop.x;
            float areaRightCopy = _cameraRightBottom.x;
            MoveXOnly();

            float leftDifference = _cameraLeftTop.x - areaLeftCopy;
            float rightDifference = _cameraRightBottom.x - areaRightCopy;
            _cameraLeftTop.x = areaLeftCopy;
            _cameraRightBottom.x = areaRightCopy;
            MoveYOnly();

            _cameraLeftTop.x += leftDifference;
            _cameraRightBottom.x += rightDifference;
        }

        private void MoveYOnly()
        {
            if (_previousVerticalTween != null && _previousVerticalTween.IsActive())
                _previousVerticalTween.OnComplete(moveVertically);
            else 
                moveVertically();
        }

        private void MoveXOnly()
        {
            if (_previousHorizontalTween != null && _previousHorizontalTween.IsActive())
                _previousHorizontalTween.OnComplete(moveHorizontally);
            else
                moveHorizontally();
        }

        private bool IsVerticalMapEnd(Vector2 cameraLeftTop, Vector2 cameraRightBottom, float direction = 0)
        {
            if (IsNearMapBottom(cameraRightBottom.y + direction) && direction < 0) 
                return true;
            
            if (IsNearMapTop(cameraLeftTop.y + direction) && direction > 0) 
                return true;
            
            return false;
        }

        private bool IsHorizontalMapEnd(Vector2 cameraLeftTop, Vector2 cameraRightBottom, float direction = 0)
        {
            if (IsNearMapLeft(cameraLeftTop.x + direction) && direction < 0) return true;
            if (IsNearMapRight(cameraRightBottom.x + direction) && direction > 0) return true;
            return false;
        }

        private void OffsetAreaVertically(float direction)
        {
            if (IsVerticalMapEnd(_cameraLeftTop, _cameraRightBottom, direction))
                return;
            
            _cameraLeftTop.y += direction;
            _cameraRightBottom.y += direction;
        }

        private void OffsetAreaHorizontally(float direction)
        {
            if (IsHorizontalMapEnd(_cameraLeftTop, _cameraRightBottom, direction))
                return;
            
            _cameraLeftTop.x += direction;
            _cameraRightBottom.x += direction;
        }

        private float GetYScaledAreaOffset(int decrement)
        {
            if (decrement == 0) return GetDirectionOfScaledArea(GetYOffsetWithAngles, decrement);
            return GetDirectionOfScaledArea(GetYOffset, decrement);
        }

        private float GetXScaledAreaOffset(int decrement)
        {
            if (decrement == 0) return GetDirectionOfScaledArea(GetXOffsetWithAngles, 0);
            return GetDirectionOfScaledArea(GetXOffset, decrement);
        }

        private float GetDirectionOfScaledArea(Func<Vector2Int, Vector2, Vector2, float> getMoveDirection, int decrement = 1)
        {
            Vector2 newLeftTop = AreaLeftTop + new Vector2(1, -1) * decrement;
            Vector2 newRightBottom = AreaRightBottom + new Vector2(-1, 1) * decrement;
            float direction = getMoveDirection(_snake.CurrentHeadCell, newLeftTop, newRightBottom);
            return direction;
        }

        private float GetXOffsetWithAngles(Vector2Int cell, Vector2 leftTop, Vector2 rightBottom)
        {
            float horizontalDirection = GetXOffset(cell, leftTop, rightBottom);
            if (horizontalDirection != 0) return horizontalDirection;
            
            float verticalDirection = GetYOffset(cell, leftTop, rightBottom);
            if (verticalDirection == 0) 
                return 0;
            
            if (GetXOffset(cell, leftTop, rightBottom + Vector2.left) > 0) 
                return 1;
            
            if (GetXOffset(cell, leftTop + Vector2.right, rightBottom) < 0) 
                return -1;
            
            return 0;
        }

        private float GetYOffsetWithAngles(Vector2Int cell, Vector2 leftTop, Vector2 rightBottom)
        {
            float verticalDirection = GetYOffset(cell, leftTop, rightBottom);
            if (verticalDirection != 0) 
                return verticalDirection;
            
            float horizontalDirection = GetXOffset(cell, leftTop, rightBottom);
            if (horizontalDirection == 0) 
                return 0;
            
            if (GetYOffset(cell, leftTop + Vector2.down, rightBottom) > 0) 
                return 1;
            
            if (GetYOffset(cell, leftTop, rightBottom + Vector2.up) < 0) 
                return -1;
            
            return 0;
        }

        private float GetXOffset(Vector2Int cell, Vector2 leftTop, Vector2 rightBottom)
        {
            Vector2 cellCorner = GetCellCorner(cell);
            if (IsRighterThanArea(Math.Min(cellCorner.x + MazeConstants.CellLength, rightBottom.x), rightBottom.x))
            {
                if (IsHorizontalMapEnd(_cameraLeftTop, _cameraRightBottom, 1))
                    return 0;
                return 1;
            }

            if (IsLefterThanArea(Math.Max(cellCorner.x, leftTop.x), leftTop.x))
            {
                if (IsHorizontalMapEnd(_cameraLeftTop, _cameraRightBottom, -1))
                    return 0;
                return -1;
            }

            return 0;
        }

        private float GetYOffset(Vector2Int cell, Vector2 leftTop, Vector2 rightBottom)
        {
            Vector2 cellCorner = GetCellCorner(cell);
            if (IsUnderArea(Math.Max(cellCorner.y - MazeConstants.CellLength, rightBottom.y), rightBottom.y))
            {
                if (IsVerticalMapEnd(_cameraLeftTop, _cameraRightBottom, -1))
                    return 0;
                return -1;
            }

            if (IsAboveArea(Math.Min(cellCorner.y, leftTop.y), leftTop.y))
            {
                if (IsVerticalMapEnd(_cameraLeftTop, _cameraRightBottom, 1))
                    return 0;
                return 1;
            }

            return 0;
        }

        private TweenerCore<Vector3, Vector3, VectorOptions> MoveNext(Func<int, float> getOffset, Action<float> offsetArea,
            Func<Ease, TweenerCore<Vector3, Vector3, VectorOptions>> moveToAreaCenter, Func<Vector2, Vector2, float, bool> isMapEnd, 
            ref bool isMoving, ref float previousDirection)
        {
            float direction = getOffset(0);
            float scaledDirection = getOffset(1);

            if (isMapEnd(_cameraLeftTop, _cameraRightBottom, scaledDirection) || isMapEnd(_cameraLeftTop, _cameraRightBottom, direction))
            {
                isMoving = false;
                offsetArea(previousDirection = direction != 0 ? direction : scaledDirection);
                return moveToAreaCenter(Ease.OutSine);
            }

            if (direction == 0 && (scaledDirection == 0 || isMoving == false))
            {
                if (!isMoving) return null;

                offsetArea(previousDirection);
                isMoving = false;
                return moveToAreaCenter(Ease.OutSine);
            }
            
            if (isMoving)
            {
                if (direction != 0)
                {
                    offsetArea(previousDirection = direction);
                    return moveToAreaCenter(Ease.Linear);
                }

                offsetArea(previousDirection = scaledDirection);
                return moveToAreaCenter(Ease.Linear);
            }

            isMoving = true;
            offsetArea(previousDirection = direction);
            return moveToAreaCenter(Ease.InSine);
        }

        private bool IsNearMapBottom(float cameraBottom) => 
            cameraBottom < GetPositionTop(-1);

        private bool IsNearMapTop(float cameraTop) => 
            cameraTop > GetPositionTop(_grid.Length + _cam.orthographicSize / _cameraScale.DefaultOrthographicSize - 1);

        private bool IsNearMapRight(float cameraRight) => 
            cameraRight > GetPositionLeft(_grid.Length);

        private bool IsNearMapLeft(float cameraLeft) => 
            cameraLeft < GetPositionLeft(0);

        private TweenerCore<Vector3, Vector3, VectorOptions> MoveXToAreaCenter(Ease ease = Ease.Linear)
        {
            float positionToMove = NextCameraPosition.x;
            float minPosition = GetPositionLeft(0) + _cameraScale.WidthUnits / 2f;
            float maxPosition = GetPositionLeft(_grid.Length) - _cameraScale.WidthUnits / 2f;
            if (Math.Abs(NextCameraPosition.x - maxPosition) < MazeConstants.CellLength)
            {
                positionToMove = maxPosition;
                ease = Ease.OutSine;
            }
            else if (Math.Abs(NextCameraPosition.x - minPosition) < MazeConstants.CellLength)
            {
                positionToMove = minPosition;
                ease = Ease.OutSine;
            }

            float distance = Math.Abs(positionToMove - transform.position.x);
            return transform.DOMoveX(positionToMove, GetMoveTime(ease, distance)).SetEase(ease);
        }

        private TweenerCore<Vector3, Vector3, VectorOptions> MoveYToAreaCenter(Ease ease = Ease.Linear)
        {
            float positionToMove = NextCameraPosition.y;
            float minPosition = GetPositionTop(-1) + _cameraScale.HeightUnits / 2f;
            float maxPosition = GetPositionTop(_grid.Length + _cam.orthographicSize / _cameraScale.DefaultOrthographicSize - 1) - _cameraScale.HeightUnits / 2f;
            if (Math.Abs(NextCameraPosition.y - maxPosition) < MazeConstants.CellLength)
            {
                positionToMove = maxPosition;
                ease = Ease.OutSine;
            }
            else if (Math.Abs(NextCameraPosition.y - minPosition) < MazeConstants.CellLength)
            {
                positionToMove = minPosition;
                ease = Ease.OutSine;
            }                       

            float distance = Math.Abs(positionToMove - transform.position.y);
            return transform.DOMoveY(positionToMove, GetMoveTime(ease, distance)).SetEase(ease);
        }

        private float GetMoveTime(Ease ease, float distance = MazeConstants.CellLength)
        {
            float standardTime = _snakeSettings.TimeAfterMiddleFrame + _snakeSettings.TimeBeforeMiddleFrame * distance;
            if (ease == Ease.OutSine) return standardTime * 1.5f;
            if (ease == Ease.InSine) return standardTime * 1.2f;
            return standardTime;
        }

        private void CountCurrentArea()
        {
            Vector2 horizontalOffset = Vector2.left * (int) Math.Ceiling((_cameraScale.WidthUnits)* MazeConstants.CellLength / 2f);
            Vector2 verticalOffset = Vector2.up * (int) Math.Ceiling((_cameraScale.HeightUnits) * MazeConstants.CellLength / 2f);
            _cameraLeftTop = GetCellCorner(_snake.CurrentHeadCell) + horizontalOffset + verticalOffset;
            PushAreaInside();
        }

        private void PushAreaInside()
        {
            _cameraLeftTop.x = Math.Max(GetPositionLeft(0), _cameraLeftTop.x);
            _cameraLeftTop.y = Math.Min(GetPositionTop(_grid.Length + _cam.orthographicSize / _cameraScale.DefaultOrthographicSize - 1), _cameraLeftTop.y);
            Vector2 rightOffset = Vector2.right * _cameraScale.WidthUnits;
            Vector2 bottomOffset = Vector2.down * _cameraScale.HeightUnits;
            _cameraRightBottom = _cameraLeftTop + rightOffset + bottomOffset;
            _cameraRightBottom.x = Math.Min(GetPositionLeft(_grid.Length), _cameraRightBottom.x);
            _cameraRightBottom.y = Math.Max(GetPositionTop(-1), _cameraRightBottom.y);
            _cameraLeftTop = _cameraRightBottom - rightOffset - bottomOffset;
        }

        private Vector2 GetCellCorner(Vector2Int cell)
        {
            Vector2 cellPosition = _gridPositioner.GetPosition(cell);
            float cornerX = GetPositionLeft(cellPosition.x);
            float cornerY = GetPositionTop(cellPosition.y);
            return new Vector2(cornerX, cornerY);
        }

        private float GetSectorsAmount(float cellsAmount) => 
            (cellsAmount - 1) / (MazeConstants.DefaultSector + 1);

        private bool IsUnderArea(float cellCornerY, float bottomBorder) => 
            cellCornerY <= bottomBorder;

        private bool IsRighterThanArea(float cellCornerX, float rightBorder) => 
            cellCornerX >= rightBorder;

        private bool IsAboveArea(float cellCornerY, float topBorder) => 
            cellCornerY >= topBorder;

        private bool IsLefterThanArea(float cellCornerX, float leftBorder) => 
            cellCornerX <= leftBorder;

        private static float GetPositionLeft(float x) => x - MazeConstants.CellLength / 2f;

        private static float GetPositionTop(float y) => y + MazeConstants.CellLength / 2f;
     }
}