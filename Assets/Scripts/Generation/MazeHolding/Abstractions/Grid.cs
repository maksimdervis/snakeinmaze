using System;
using Generation.Cells;
using Generation.Cells.Abstractions;
using UnityEngine;

namespace Generation.MazeHolding.Abstractions
{
    public abstract class Grid : MonoBehaviour
    {
        public int Length =>
            _grid.GetLength(0);

        public Vector2Int StartCell
        {
            get
            {
                if (_isGenerated == false)
                    Generate();
                return _startCell;
            }
            protected set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value), "Start cell value cannot be null");
                _startCell = value;
            }
        }


        protected Cell[,] _grid;
        protected bool[,] _isFood;

        public int LeftFoodNumber
        {
            get => leftFoodNumber;
            protected set
            {
                leftFoodNumber = value;
                if (leftFoodNumber < 0)
                    throw new ArgumentOutOfRangeException(nameof(value), "Food number can't be negative");
            }
        }

        private Vector2Int _startCell; 
        private bool _isGenerated;
        private int leftFoodNumber;

        public Cell GetCellAt(Vector2Int at)
        {
            if (_isGenerated == false)
                Generate();
            return _grid[at.x, at.y];
        }
        
        public bool ContainsFoodAt(Vector2Int at)
        {
            if (_isGenerated == false)
                Generate();
            return _isFood[at.x, at.y];
        }

        protected virtual void Generate() => 
            _isGenerated = true;

        public void EatFoodAt(Vector2Int at) => 
            _isFood[at.x, at.y] = false;
    }
}
