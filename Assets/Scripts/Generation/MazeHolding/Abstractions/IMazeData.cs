﻿using UnityEngine;

namespace Generation.MazeHolding.Abstractions
{
    public interface IMazeData
    {
        int Level { get; }
        int SectorsPerSide { get; }
        int FoodAmount { get; }
    }
}