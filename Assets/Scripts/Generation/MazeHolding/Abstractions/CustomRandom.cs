﻿namespace Generation.MazeHolding.Abstractions
{
    public abstract class CustomRandom<T>
    {
        public abstract T GetRandom(T minInclusive, T maxExclusive);
    }
}
