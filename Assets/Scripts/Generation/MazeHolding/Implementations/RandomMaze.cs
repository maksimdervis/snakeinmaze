﻿using System;
using System.Collections.Generic;
using System.Linq;
using Constants;
using Enums;
using Extensions;
using Generation.Cells.Implementations;
using ModestTree;
using UnityEngine;

namespace Generation.MazeHolding.Implementations
{
    public class RandomMaze : RandomGrid
    {
        private Vector2Int _startSector;
        private Vector2Int _finishSector;

        protected override void Generate()
        {
            base.Generate();
            _startSector = ChooseStartSector();
            FormMaze();
            PlaceFood();
            ClearSector(_startSector);
            ClearSector(_finishSector);

            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
            {
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                {
                    for (int xOffset = 0; xOffset < MazeConstants.MinSector; xOffset++)
                    {
                        for (int yOffset = 0; yOffset < MazeConstants.MinSector; yOffset++)
                        {
                            int xPosition = _groundAreas[xSector, ySector].x + xOffset;
                            int yPosition = _groundAreas[xSector, ySector].y + yOffset;
                            if (_grid[xPosition, yPosition] == wall)
                                continue;
                            
                            _grid[xPosition, yPosition] = redWall;
                        }
                    }
                }
            }
            
            PlaceExit(_finishSector);
        }

        private void PlaceExit(Vector2Int finishSector)
        {
            Vector2Int cell = GetMostCenteredCell(finishSector);
            _grid[cell.x, cell.y] = exit;
        }

        private Vector2Int GetMostCenteredCell(Vector2Int sector)
        {
            int left = MazeConstants.GetSectorFirstCell(sector.x);
            int right = left + MazeConstants.DefaultSector - 1;
            int top = MazeConstants.GetSectorFirstCell(sector.y);
            int bottom = top + MazeConstants.DefaultSector - 1;
            int exitX = (left + right) / 2 + Convert.ToInt32(GetSectorWidth(sector) % 2 == 0) * RandomInstance.GetRandom(0, 1 + 1);
            if (GetSectorWidth(sector) == MazeConstants.MinSector)
            {
                bool leftNearPassage = false;
                for (int y = top; y <= bottom; y++)
                    if (_grid[left, y].GetType() == typeof(WallGround))
                    {
                        leftNearPassage = true;
                        break;
                    }

                exitX = leftNearPassage ? right : left;
            }

            int exitY = (top + bottom) / 2 + Convert.ToInt32(GetSectorHeight(sector) % 2 == 0) * RandomInstance.GetRandom(0, 1 + 1);
            if (GetSectorHeight(sector) == MazeConstants.MinSector)
            {
                bool topNearPassage = false;
                for (int x = left; x <= right; x++)
                    if (_grid[x, top].GetType() == typeof(WallGround))
                    {
                        topNearPassage = true;
                        break;
                    }

                exitY = topNearPassage ? bottom : top;
            }

            return new Vector2Int(exitX, exitY);
        }

        private Vector2Int ChooseStartSector()
        {
            int x = RandomInstance.GetRandom(0, SectorsPerSide);
            int y = RandomInstance.GetRandom(0, SectorsPerSide);
            Vector2Int current = new Vector2Int(x, y);
            StartCell = GetMostCenteredCell(current);
            return current;
        }
        
        private void PlaceFood()
        {
            List<Vector2Int> sectorsForFood = new List<Vector2Int>();
            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                    if (IsSectorWithFood(xSector, ySector))
                        sectorsForFood.Add(new Vector2Int(xSector, ySector));
            
            for (int i = 0; i < LeftFoodNumber; i++)
            {
                if (sectorsForFood.Count == 0)
                    break;
                
                int randomIndex = RandomInstance.GetRandom(0, sectorsForFood.Count);
                Vector2Int foodSector = sectorsForFood[randomIndex];
                sectorsForFood.RemoveAt(randomIndex);
                Vector2Int foodPosition = GetRandomGroundAreaCell(foodSector);
                _isFood[foodPosition.x, foodPosition.y] = true; 
            }
        }

        private bool IsSectorWithFood(int xSector, int ySector)
        {
            return (_startSector.x != xSector || _startSector.y != ySector) && 
                   (_finishSector.x != xSector || _finishSector.y != ySector);
        }

        private void FormMaze()
        {
            List<Vector2Int> sectors = GetSectorsList();
            List<Vector2Int> path = GetRandomPath(_startSector);
            AddLeftSectorsToPath(path);
            _finishSector = path[^1];
            RemoveWallsOnPath(path);
            OpenSingleSectors(sectors, _startSector, path);
        }

        private void OpenSingleSectors(List<Vector2Int> sectors, Vector2Int startSector, List<Vector2Int> path)
        {
            List<Vector2Int> leftSectors = sectors.Where(value => path.Contains(value) == false).ToList();
            foreach (Vector2Int sector in leftSectors)
            {
                List<Direction> directions = GetDirections(sector);
                for (int i = 0; i < directions.Count; i++)
                {
                    Vector2Int neighbour = sector + directions[i].ToVector2Int();
                    if (directions.Count <= 1)
                        break;
                    
                    if (path.Contains(neighbour) == false || neighbour == startSector)
                        directions.RemoveAt(i--);
                }

                Direction randomDirection = directions[RandomInstance.GetRandom(0, directions.Count)];
                MakeHole(sector, randomDirection, Vector2Int.zero, true);
            }
        }

        private void RemoveWallsOnPath(List<Vector2Int> path)
        {
            Vector2Int previousPassage = Vector2Int.zero;
            for (int i = 0; i < path.Count - 1; i++)
            {
                Direction direction = DirectionExtensions.GetDirection(path[i + 1] - path[i]);
                previousPassage = MakeHole(path[i], direction, previousPassage);
            }
        }

        private void AddLeftSectorsToPath(List<Vector2Int> path)
        {
            bool extend;
            do
            {
                var pathIndexes = new List<int>();
                for (int i = 0; i < path.Count - 1; i++) 
                    pathIndexes.Add(i);
                
                int indexIndex = RandomInstance.GetRandom(0, pathIndexes.Count);
                extend = TryExtendPath(path, pathIndexes[indexIndex]); 
                pathIndexes.RemoveAt(indexIndex);
                while (extend == false && pathIndexes.IsEmpty() == false)
                {
                    indexIndex = RandomInstance.GetRandom(0, pathIndexes.Count);
                    extend = TryExtendPath(path, pathIndexes[indexIndex]);
                    pathIndexes.RemoveAt(indexIndex);
                }
            } while (extend);
        }

        private List<Vector2Int> GetRandomPath(Vector2Int start)
        {
            List<Vector2Int> path = new List<Vector2Int> {start};
            Vector2Int current = start;
            Direction previousDirection = Direction.Down;
            do
            {
                List<Direction> directions = GetDirections(current);
                for (int i = 0; i < directions.Count; i++)
                    if (path.Contains(current + directions[i].ToVector2Int()))
                        directions.RemoveAt(i--);
                if (directions.Count > 1)
                    directions.Remove(previousDirection);
                if (directions.Count == 0)
                    break;
                
                Direction direction = directions[RandomInstance.GetRandom(0, directions.Count)];
                Vector2Int neighbour = current + direction.ToVector2Int();
                current = neighbour;
                path.Add(neighbour);
                previousDirection = direction;
            } 
            while (true);
            
            return path;
        }

        private List<Vector2Int> GetSectorsList()
        {
            List<Vector2Int> sectors = new List<Vector2Int>();
            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                    sectors.Add(new Vector2Int(xSector, ySector));
            return sectors;
        }

        private bool TryExtendPath(List<Vector2Int> path, int index)
        {
            if (!Bfs(in path, path[index], path[index + 1], out List<Vector2Int> found)) 
                return false;
            
            foreach (Vector2Int sector in found)
                path.Insert(++index, sector);

            return true;
        }
        
        private bool Bfs
                        (in List<Vector2Int> used, 
                        Vector2Int start, 
                        Vector2Int finish, 
                        out List<Vector2Int> path)
        {
            var foundPath = false; 
            var parent = new Dictionary<Vector2Int, Vector2Int>();
            var marked = new HashSet<Vector2Int> {start};
            var q = new Queue<Vector2Int>();
            
            q.Enqueue(start);
            while (q.IsEmpty() == false)
            {
                Vector2Int current = q.Dequeue();
                List<Direction> directions = GetDirections(current);
                directions.Shuffle(RandomInstance);
                
                foreach (Direction direction in directions)
                {
                    Vector2Int neighbour = current + direction.ToVector2Int();
                    if (current != start && neighbour == finish)
                    {
                        parent[finish] = current;
                        foundPath = true;
                        // break all loops 
                        q.Clear();  
                        break;
                    }
            
                    if (marked.Contains(neighbour) || used.Contains(neighbour)) 
                        continue;
                    
                    marked.Add(neighbour);
                    q.Enqueue(neighbour);
                    parent[neighbour] = current;
                }
            }

            if (foundPath == false)
            {
                path = null;
                return false;
            }

            path = new List<Vector2Int>();
            Vector2Int position = parent[finish];
            while (position != start)
            {
                path.Add(position);
                position = parent[position];
            }
            path.Reverse();
            return true; 
        }

        private List<Direction> GetDirections(Vector2Int sector)
        {
            var directions = new List<Direction>();
            foreach (Direction direction in Enum.GetValues(typeof(Direction)))
            {
                Vector2Int neighbour = GetSectorOnDirection(sector, direction);
                if (IsCorrectSector(neighbour))
                    directions.Add(direction);
            }
            
            return directions;
        }

        private bool IsCorrectSector(Vector2Int sector) =>
            sector.x < SectorsPerSide && sector.y < SectorsPerSide && sector.x >= 0 && sector.y >= 0;

        private static Vector2Int GetSectorOnDirection(Vector2Int sector, Direction direction) => 
            sector + direction.ToVector2Int();
    }
}