﻿using System;
using System.Collections.Generic;
using System.Linq;
using Constants;
using Enums;
using Generation.Cells;
using Generation.Cells.Abstractions;
using Generation.Cells.Implementations;
using Generation.MazeHolding.Abstractions;
using UnityEngine;
using Zenject;
using Grid = Generation.MazeHolding.Abstractions.Grid;

namespace Generation.MazeHolding.Implementations
{
    public class RandomGrid : Grid
    {
        [SerializeField] private Ground ground;
        [SerializeField] protected Wall wall;
        [SerializeField] protected RedWall redWall;
        [SerializeField] protected Exit exit; 
        [SerializeField] private WallGround wallGround;
        
        private Vector2Int[,] _leftTop;
        private Dictionary<Direction, List<Vector2Int>>[,] _sectorWalls;

        protected int SectorsPerSide { get; private set; }
        protected CustomRandom<int> RandomInstance { get; private set; }

        protected Vector2Int[,] _groundAreas;

        [Inject]
        public void Construct(IMazeData mazeData, CustomRandom<int> customRandom)
        {
            RandomInstance = customRandom; 
            SectorsPerSide = mazeData.SectorsPerSide;
            LeftFoodNumber = mazeData.FoodAmount;
            
            int gridLength = MazeConstants.GetLength(SectorsPerSide);
            _grid = new Cell[gridLength, gridLength];
            _isFood = new bool[gridLength, gridLength]; 
            GetGroundAreaStarts(out _groundAreas);
            
            _sectorWalls = new Dictionary<Direction, List<Vector2Int>>[gridLength, gridLength];
            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                    _sectorWalls[xSector, ySector] = new Dictionary<Direction, List<Vector2Int>>
                    {
                        [Direction.Down] = new(),
                        [Direction.Up] = new(),
                        [Direction.Right] = new(),
                        [Direction.Left] = new()
                    };
        }

        protected override void Generate()
        {
            base.Generate();
            SetUpEmptyLevel();
            SetUpSectors();
            IntersectAllWalls();
        }

        protected Vector2Int GetRandomGroundAreaCell(Vector2Int sector)
        {
            int left = _groundAreas[sector.x, sector.y].x;
            int top = _groundAreas[sector.x, sector.y].y;
            var cells = new List<Vector2Int>();
            for (int x = left; x <= left + 1; x++) 
                for (int y = top; y <= top + 1; y++)
                    if (_grid[x, y] != wall && IsNearCorner(new Vector2Int(x, y)) == false)
                        cells.Add(new Vector2Int(x, y));
            Vector2Int returnCell = cells[0];
            int minNeighborWalls = GetNumberOfNeighborWalls(returnCell); 
            for (int i = 1; i < cells.Count; i++)
            {
                int currentNeighborsNumber = GetNumberOfNeighborWalls(cells[i]); 
                if (currentNeighborsNumber < minNeighborWalls)
                {
                    returnCell = cells[i];
                    minNeighborWalls = currentNeighborsNumber; 
                }
            }

            return returnCell;
        }

        private bool IsNearCorner(Vector2Int cell)
        {
            int[] offsets = {-1, 1};
            foreach (int offset in offsets)
            {
                if (_grid[cell.x + offset, cell.y] == wall && _grid[cell.x, cell.y + offset] == wall)
                    return true; 
                if (_grid[cell.x + offset, cell.y] == wall && _grid[cell.x, cell.y - offset] == wall)
                    return true; 
            }
            
            return false;
        }

        private int GetNumberOfNeighborWalls(Vector2Int cell)
        {
            return Convert.ToInt32(_grid[cell.x + 1, cell.y] == wall) + 
                    Convert.ToInt32(_grid[cell.x - 1, cell.y] == wall) + 
                    Convert.ToInt32(_grid[cell.x, cell.y + 1] == wall) + 
                    Convert.ToInt32(_grid[cell.x, cell.y - 1] == wall);
        }

        protected Vector2Int GetRandomCellOfSector(Vector2Int sector)
        {
            int x = GetRandomSectorCellX(sector);
            int y = GetRandomSectorCellY(sector);
            return new Vector2Int(x, y);
        }

        private int GetRandomSectorCellY(Vector2Int sector)
        {
            int minY = _sectorWalls[sector.x, sector.y][Direction.Up][0].y + 1;
            int maxY = _sectorWalls[sector.x, sector.y][Direction.Down][0].y - 1;
            int y = RandomInstance.GetRandom(minY, maxY + 1);
            return y;
        }

        private int GetRandomSectorCellX(Vector2Int sector)
        {
            int minX = _sectorWalls[sector.x, sector.y][Direction.Left][0].x + 1;
            int maxX = _sectorWalls[sector.x, sector.y][Direction.Right][0].x - 1;
            int x = RandomInstance.GetRandom(minX, maxX + 1);
            return x;
        }

        protected int GetSectorHeight(Vector2Int sector) => 
            _leftTop[sector.x, sector.y + 1].y - _leftTop[sector.x, sector.y].y - 1;

        protected int GetSectorWidth(Vector2Int sector) => 
            _leftTop[sector.x + 1, sector.y].x - _leftTop[sector.x, sector.y].x - 1;

        protected void ClearSector(Vector2Int sector)
        {
            for (int x = _leftTop[sector.x, sector.y].x + 1; x < _leftTop[sector.x + 1, sector.y].x; x++)
                for (int y = _leftTop[sector.x, sector.y].y + 1; y < _leftTop[sector.x, sector.y + 1].y; y++)
                    _grid[x, y] = ground;
        }
        
        protected Vector2Int MakeHole(Vector2Int sector, Direction direction, Vector2Int previous, bool isDeadEnd = false)
        {
            List<Vector2Int> walls = new List<Vector2Int>(_sectorWalls[sector.x, sector.y][direction]);
            if (walls.Count == MazeConstants.MinSector)
            {
                if (isDeadEnd)
                {
                    foreach (Vector2Int w in walls)
                        _grid[w.x, w.y] = wallGround;
                    return walls[0];
                }

                float firstWallDistance = Vector2Int.Distance(walls[0], previous);
                float secondWallDistance = Vector2Int.Distance(walls[1], previous);
                Vector2Int passage = firstWallDistance > secondWallDistance ? walls[0] : walls[1];
                _grid[passage.x, passage.y] = wallGround;
                return passage;
            }

            if (walls.Count == MazeConstants.DefaultSector)
            {
                int minHolesNumber = isDeadEnd ? 2 : 1;
                int numberOfHoles = RandomInstance.GetRandom(minHolesNumber, 2 + 1);
                
                _grid[walls[1].x, walls[1].y] = wallGround; 
                walls.RemoveAt(1);
                
                Vector2Int returnHole = Vector2Int.zero;
                for (int i = 0; i < numberOfHoles - 1; i++)
                {
                    Vector2Int distant = walls[0];
                    for (int j = 1; j < walls.Count; j++)
                    {
                        float newDistance = Vector2Int.Distance(walls[j], previous);
                        float oldDistance = Vector2Int.Distance(distant, previous);
                        if (newDistance > oldDistance)
                            distant = walls[j];
                    }

                    returnHole = distant;
                    walls.Remove(distant);
                    _grid[distant.x, distant.y] = wallGround;
                }
                
                return returnHole;
            }

            if (walls.Count == MazeConstants.MaxSector)
            {
                Vector2Int[][] holes = 
                {
                    new[] {walls[0], walls[1]},
                    new[] {walls[^2], walls[^1]}
                };

                float firstHoleDistance = Vector2Int.Distance(holes[0][0], previous);
                float secondHoleDistance = Vector2Int.Distance(holes[1][0], previous);
                Vector2Int[] hole = firstHoleDistance > secondHoleDistance ? holes[0] : holes[1];
                foreach (Vector2Int position in hole)
                    _grid[position.x, position.y] = wallGround;
                return hole[0];
            }
            
            throw new ArgumentException($"Incorrect wall length on direction, found values of {walls.Count} positions in sector {sector}");
        }

        private void IntersectAllWalls()
        {
            for (int xSector = 0; xSector < SectorsPerSide - 1; xSector++)
                IntersectWalls(new Vector2Int(xSector, SectorsPerSide - 1), Direction.Right);

            for (int ySector = 0; ySector < SectorsPerSide - 1; ySector++)
                IntersectWalls(new Vector2Int(SectorsPerSide - 1, ySector), Direction.Down);

            for (int xSector = 0; xSector < SectorsPerSide - 1; xSector++)
            {
                for (int ySector = 0; ySector < SectorsPerSide - 1; ySector++)
                {
                    IntersectWalls(new Vector2Int(xSector, ySector), Direction.Right);
                    IntersectWalls(new Vector2Int(xSector, ySector), Direction.Down);
                }
            }
        }
        
        private void IntersectWalls(Vector2Int sector, Direction direction)
        {
            Direction oppositeDirection = direction.GetOpposite();
            Vector2Int vectorDirection = direction.ToVector2Int();
            
            List<Vector2Int> firstWall = _sectorWalls[sector.x, sector.y][direction];
            List<Vector2Int> secondWall = _sectorWalls[sector.x + vectorDirection.x, sector.y + vectorDirection.y][oppositeDirection];

            List<Vector2Int> wallsListsIntersection = firstWall.Intersect(secondWall).ToList();

            _sectorWalls[sector.x, sector.y][direction] = wallsListsIntersection;
            _sectorWalls[sector.x + vectorDirection.x, sector.y + vectorDirection.y][oppositeDirection] = wallsListsIntersection;
        }

        private void SetUpEmptyLevel()
        {
            for (int x = 0; x < Length; x++)
                for (int y = 0; y < Length; y++)
                    if (x == 0 || y == 0 || x == Length - 1 || y == Length - 1)
                        _grid[x, y] = wall;
                    else
                        _grid[x, y] = ground;
        }
        
        
        private void SetUpSectors()
        {
            GetSectorsLeftTopCorners(out _leftTop);
            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                    PlaceSector(new Vector2Int(xSector, ySector));
        }

        private void GetSectorsLeftTopCorners(out Vector2Int[,] leftTop)
        {
            leftTop = new Vector2Int[SectorsPerSide + 1, SectorsPerSide + 1];
            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
            {
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                {
                    Vector2Int sector = new Vector2Int(xSector, ySector); 
                    if (xSector < SectorsPerSide - 1)
                    {
                        int rightSectorLeftPosition = GetRightWall(sector);
                        leftTop[xSector + 1, ySector].x = rightSectorLeftPosition;
                    }

                    if (ySector < SectorsPerSide - 1)
                    {
                        int bottomSectorTopPos = GetBottomWall(sector);
                        leftTop[xSector, ySector + 1].y = bottomSectorTopPos;
                    }
                }
            }

            for (int sector = 0; sector < SectorsPerSide; sector++)
            {
                leftTop[SectorsPerSide, sector].Set(Length - 1, leftTop[SectorsPerSide - 1, sector].y);
                leftTop[sector, SectorsPerSide].Set(leftTop[sector, SectorsPerSide - 1].x, Length - 1);
            }
        }

        private int GetBottomWall(Vector2Int sector)
        {
            if (sector.x == 0 || _leftTop[sector.x, sector.y + 1].x == _leftTop[sector.x, sector.y].x)
            {
                int nextGroundArea = _groundAreas[sector.x, sector.y + 1].y;
                int currentGroundArea = _groundAreas[sector.x, sector.y].y;
                (int min, int max) bottomWallRange = GetNextWallRange(_leftTop[sector.x, sector.y].y, currentGroundArea, nextGroundArea);

                int bannedBottomPosition = -1;
                if (sector.y > 0)
                {
                    int previousDistanceBetweenWalls = _leftTop[sector.x, sector.y].y - _leftTop[sector.x, sector.y - 1].y;
                    bannedBottomPosition = _leftTop[sector.x, sector.y].y + previousDistanceBetweenWalls;
                }

                int width = _leftTop[sector.x + 1, sector.y].x - _leftTop[sector.x, sector.y].x;
                return TryGetRandomWithoutValues(bottomWallRange, bannedBottomPosition, _leftTop[sector.x, 
                sector.y].y + width);
            }

            int returnValue = _leftTop[sector.x - 1, sector.y + 1].y;
            _groundAreas[sector.x, sector.y].y = Math.Min(returnValue - 2, _groundAreas[sector.x, sector.y].y);
            return returnValue;
        }

        private int GetRightWall(Vector2Int sector)
        {
            int groundArea = _groundAreas[sector.x, sector.y].x;
            if (sector.y != 0 && IsOnHorizontalLineWithGroundArea(groundArea, _groundAreas[sector.x + 1, sector.y - 1].x))
            {
                int returnValue = _leftTop[sector.x + 1, sector.y - 1].x;
                _groundAreas[sector.x, sector.y].x = Math.Min(returnValue - MazeConstants.MinSector, _groundAreas[sector.x, sector.y].x);
                return returnValue;
            }

            int nextGroundArea = _groundAreas[sector.x + 1, sector.y].x;
            (int min, int max) nextWallRange = GetNextWallRange(_leftTop[sector.x, sector.y].x, groundArea, nextGroundArea);
            if (sector.x == 0)
                return GetRandomValue(nextWallRange);

            int previousWidth = _leftTop[sector.x, sector.y].x - _leftTop[sector.x - 1, sector.y].x;
            int banned = _leftTop[sector.x, sector.y].x + previousWidth;
            return TryGetRandomWithoutValues(nextWallRange, banned);
        }

        private void PlaceSector(Vector2Int sector)
        {
            int left = _leftTop[sector.x, sector.y].x;
            int right = _leftTop[sector.x + 1, sector.y].x;
            int top = _leftTop[sector.x, sector.y].y;
            int bottom = _leftTop[sector.x, sector.y + 1].y;

            for (int x = left + 1; x < right; x++)
                for (int y = top + 1; y < bottom; y++)
                    _grid[x, y] = ground;

            _grid[left, top] = wall;
            _grid[left, bottom] = wall;
            _grid[right, top] = wall;
            _grid[right, bottom] = wall;

            for (int x = left + 1; x < right; x++)
            {
                _grid[x, top] = wall;
                _sectorWalls[sector.x, sector.y][Direction.Up].Add(new Vector2Int(x, top));

                _grid[x, bottom] = wall;
                _sectorWalls[sector.x, sector.y][Direction.Down].Add(new Vector2Int(x, bottom));
            }

            for (int y = top + 1; y < bottom; y++)
            {
                _grid[left, y] = wall;
                _sectorWalls[sector.x, sector.y][Direction.Left].Add(new Vector2Int(left, y));

                _grid[right, y] = wall;
                _sectorWalls[sector.x, sector.y][Direction.Right].Add(new Vector2Int(right, y));
            }
            
            AddAdditionalWalls(right, left, bottom, top);
        }

        private void AddAdditionalWalls(int right, int left, int bottom, int top)
        {
            int width = right - left - 1;
            int height = bottom - top - 1;
            
            for (int x = left + 2; x < right - 1; x++)
                for (int y = top + 2; y < bottom - 1; y++)
                    _grid[x, y] = wall;

            bool isMaxRoom = width == 4 && height == 4;
            if (isMaxRoom)
            {
                int x = RandomInstance.GetRandom(left + 2, right - 1);
                int y = RandomInstance.GetRandom(top + 2, bottom - 1);
                _grid[x, y] = ground;
            }
        }

        private void GetGroundAreaStarts(out Vector2Int[,] groundAreaStarts)
        {
            Vector2Int[,] relativeStarts = new Vector2Int[SectorsPerSide, SectorsPerSide];

            relativeStarts[0, 0] = new Vector2Int(0, 0);
            relativeStarts[0, SectorsPerSide - 1] = new Vector2Int(0, 1);
            relativeStarts[SectorsPerSide - 1, 0] = new Vector2Int(1, 0);
            relativeStarts[SectorsPerSide - 1, SectorsPerSide - 1] = new Vector2Int(1, 1);

            for (int position = 1; position < SectorsPerSide - 1; position++)
            {
                relativeStarts[0, position] = new Vector2Int(0, MazeConstants.DefaultSector - MazeConstants.MinSector);
                relativeStarts[SectorsPerSide - 1, position] = new Vector2Int(1, MazeConstants.DefaultSector - MazeConstants.MinSector);
                relativeStarts[position, 0] = new Vector2Int(MazeConstants.DefaultSector - MazeConstants.MinSector, 0);
                relativeStarts[position, SectorsPerSide - 1] = new Vector2Int(MazeConstants.DefaultSector - MazeConstants.MinSector, 1);
            }

            for (int xSector = 1; xSector < SectorsPerSide - 1; xSector++)
                for (int ySector = 1; ySector < SectorsPerSide - 1; ySector++)
                    relativeStarts[xSector, ySector] 
                        = new Vector2Int(RandomInstance.GetRandom(0, MazeConstants.DefaultSector - MazeConstants.MinSector + 1),
                                        RandomInstance.GetRandom(0, MazeConstants.DefaultSector - MazeConstants.MinSector + 1));

            groundAreaStarts = new Vector2Int[SectorsPerSide, SectorsPerSide];
            for (int xSector = 0; xSector < SectorsPerSide; xSector++)
                for (int ySector = 0; ySector < SectorsPerSide; ySector++)
                    groundAreaStarts[xSector, ySector] = GetDefaultSectorStart(new Vector2Int(xSector, ySector)) +
                                                                                relativeStarts[xSector, ySector];
        }

        private bool IsOnHorizontalLineWithGroundArea(int position, int groundArea) => 
            position == groundArea || position == groundArea + 1;

        private int TryGetRandomWithoutValues((int min, int max) range, params int[] valuesToThrowAway)
        {
            var possibleValues = new List<int>();
            for (int value = range.min; value <= range.max; value++)
            {
                bool isThrownValue = valuesToThrowAway.Any(valueToThrow => value == valueToThrow);
                if (isThrownValue == false)
                    possibleValues.Add(value);
            }

            if (possibleValues.Count == 0)
                return GetRandomValue(range);
    
            return possibleValues[RandomInstance.GetRandom(0, possibleValues.Count)];
        }

        private int GetRandomValue((int min, int max) range) => 
            RandomInstance.GetRandom(range.min, range.max + 1);

        private (int min, int max) GetNextWallRange(int wallPosition, int currentGroundArea, int nextGroundArea)
        {
            int minPosition = currentGroundArea + MazeConstants.MinSector;
            int maxPosition = Math.Min(wallPosition + MazeConstants.MaxSector + 1, nextGroundArea - 1);
            maxPosition = Math.Min(wallPosition + MazeConstants.MaxSector, maxPosition);
            return (minPosition, maxPosition);
        }

        private Vector2Int GetDefaultSectorStart(Vector2Int sector) =>
            new((MazeConstants.CellLength + MazeConstants.DefaultSector) * sector.x + MazeConstants.CellLength, (MazeConstants.CellLength + MazeConstants.DefaultSector) * sector.y + MazeConstants.CellLength);
    }
}