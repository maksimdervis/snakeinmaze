﻿using System;
using Generation.MazeHolding.Abstractions;
using Zenject;

namespace Generation.MazeHolding.Implementations
{
    public class LevelBasedRandomizer : CustomRandom<int>
    {
        private readonly Random random; 
        
        public LevelBasedRandomizer([Inject(Id = "Level")]int level) => 
            random = new Random(level);

        public virtual int GetRandom() => 
            random.Next();

        public override int GetRandom(int minInclusive, int maxExclusive) => 
            random.Next(minInclusive, maxExclusive);

        public virtual bool ToDo() => 
            random.Next(0, 1 + 1) == 0;
    }
}