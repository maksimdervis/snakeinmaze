﻿using System;
using Constants;
using Generation.MazeHolding.Abstractions;
using UnityEngine;
using Zenject;

namespace Generation.MazeHolding.Implementations
{
    public class LevelBasedMazeData : IMazeData
    {
        public int Level { get; }
        public int SectorsPerSide { get; }
        public int FoodAmount { get; }

        public LevelBasedMazeData([Inject(Id = "Level")] int level)
        {
            SectorsPerSide = Math.Max(Convert.ToInt32(Mathf.Ceil(Mathf.Sqrt(level))), 2);
            Level = level;
            FoodAmount = level - 2;
        }
    }
}