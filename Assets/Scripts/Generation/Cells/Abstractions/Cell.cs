using System.Numerics;
using Generation.Cells.Implementations;
using Generation.MazeHolding.Abstractions;
using UnityEngine;
using Zenject;
using Vector2 = UnityEngine.Vector2;

namespace Generation.Cells.Abstractions
{
    public abstract class Cell : MonoBehaviour
    {
        [SerializeField] private int sortingOrder;
        public int SortingOrder => sortingOrder;
        
        public class CellFactory : IFactory<Cell>
        {
            private readonly DiContainer _container;
            
            public CellFactory(DiContainer container)
            {
                _container = container;
            }

            public Cell Create(Cell prefab, Vector2Int position)
            {
                Cell returnCell = _container.InstantiatePrefab(prefab).GetComponent<Cell>();
                returnCell.transform.position = (Vector2) position;
                return returnCell;
            }

            public Cell Create() =>
                _container.Instantiate<Ground>();
        }
    }
}
