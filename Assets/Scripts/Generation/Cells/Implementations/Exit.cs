﻿using System;
using Generation.Cells.Abstractions;
using Generation.MazeHolding.Abstractions;
using Generation.MazeHolding.Implementations;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Generation.Cells.Implementations
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Exit : MazeCell
    {
        [SerializeField] private Sprite openedExitSprite;
        
        private bool isOpened;
        private ZenjectSceneLoader _sceneLoader;
        private IMazeData _mazeData;

        public void Open()
        {
            isOpened = true;
            GetComponent<SpriteRenderer>().sprite = openedExitSprite; 
        }

        [Inject]
        public void Construct(ZenjectSceneLoader sceneLoader, IMazeData mazeData)
        {
            _sceneLoader = sceneLoader;
            _mazeData = mazeData;
        }

        public bool TryGoThrough()
        {
            if (isOpened == false)
                return false; 
            
            _sceneLoader.LoadSceneAsync("Game", LoadSceneMode.Single, container => 
                container.BindInstance(_mazeData.Level + 1).WithId("Level"));
            return true;
        }
    }
}