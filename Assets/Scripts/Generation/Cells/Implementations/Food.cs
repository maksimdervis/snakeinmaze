﻿using Generation.Cells.Abstractions;

namespace Generation.Cells.Implementations
{
    public class Food : Cell
    {
        public void MakeEaten() => 
            Destroy(gameObject);
    }
}