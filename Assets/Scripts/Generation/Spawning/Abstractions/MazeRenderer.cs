﻿using System;
using System.Collections.Generic;
using Generation.Cells;
using Generation.Cells.Abstractions;
using Generation.Cells.Implementations;
using UnityEngine;
using Zenject;
using Grid = Generation.MazeHolding.Abstractions.Grid;

namespace Generation.Spawning.Abstractions
{
    public abstract class MazeRenderer : MonoBehaviour
    {
        protected List<Cell>[,] Cells { get; private set; }
        protected Grid Grid { get; private set; }

        protected int LeftFoodNumber
        {
            get => _leftFoodNumber;
            set
            {
                _leftFoodNumber = value;
                _onLeftFoodNumberChanged?.Invoke(value);
            }
        }

        private Action<int> _onLeftFoodNumberChanged;
        private int _leftFoodNumber;

        [Inject]
        public void Construct(Grid grid)
        {
            _onLeftFoodNumberChanged?.Invoke(0);
            Grid = grid;
            Cells = new List<Cell>[grid.Length, grid.Length];
            for (int x = 0; x < grid.Length; x++)
                for (int y = 0; y < grid.Length; y++)
                    Cells[x, y] = new List<Cell>();
        }

        public void OnLeftFoodNumberChanged(Action<int> action) => 
            _onLeftFoodNumberChanged += action;

        public abstract void InstantiateMaze();

        public abstract bool TryExit();

        public abstract void EatFoodAt(Vector2Int at);
    }
}