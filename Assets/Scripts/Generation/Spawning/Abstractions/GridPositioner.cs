﻿using System;
using Generation.Cells;
using Generation.Cells.Abstractions;
using Generation.MazeHolding.Abstractions;
using UnityEngine;
using Zenject;
using CellFactory =  Generation.Cells.Abstractions.Cell.CellFactory;

namespace Generation.Spawning.Abstractions
{
    public abstract class GridPositioner : MonoBehaviour
    {
        protected IMazeData MazeData { get; private set; }
        protected CellFactory CellFactory { get; private set; }

        [Inject]
        public virtual void Construct(IMazeData mazeData, CellFactory cellFactory)
        {
            MazeData = mazeData;
            CellFactory = cellFactory;
        }

        public abstract TCell Instantiate<TCell>(TCell objectToInstantiate, Vector2Int cellPosition) where TCell : Cell;
        
        protected bool TrySetOrderInLayer(GameObject someGameObject, int additionalSortingOrder)
        {
            if (someGameObject.TryGetComponent(out SpriteRenderer spriteRenderer) && 
                someGameObject.TryGetComponent(out Cell cell))
            {
                spriteRenderer.sortingOrder = MazeData.SectorsPerSide * cell.SortingOrder + additionalSortingOrder;
                return true;
            }

            return false;
        }

        public abstract void Replace<TCell>(TCell objectToReplace, Vector2Int newCellPosition) where TCell : Cell;
        public abstract Vector2 GetPosition(Vector2Int cell);
    }
}