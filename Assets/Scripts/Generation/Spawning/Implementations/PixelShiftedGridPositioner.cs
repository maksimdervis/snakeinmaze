﻿// using Generation.Cells;
// using Generation.Cells.Abstractions;
// using Generation.MazeHolding.Abstractions;
// using Generation.Spawning.Abstractions;
// using UnityEngine;
// using Zenject;
// using Grid = Generation.MazeHolding.Abstractions.Grid;
// using CellFactory =  Generation.Cells.Abstractions.Cell.CellFactory;
//
// namespace Generation.Spawning.Implementations
// {
//     public class PixelShiftedGridPositioner : GridPositioner
//     {
//         [SerializeField] private float pixelsPerUnit;
//
//         private Grid _grid; 
//         
//         [Inject]
//         public void Construct(IMazeData mazeData, CellFactory cellFactory, Grid grid)
//         {
//             base.Construct(mazeData, cellFactory);
//             _grid = grid;
//         }
//         
//         public override TCell Instantiate<TCell>
//                                 (Cell objectToInstantiate,
//                                 Vector2Int cellPosition) where TCell : Cell
//         {
//             return CellFactory.Create(objectToInstantiate, cellPosition);
//         }
//
//         public override T Instantiate<T>(T objectToInstantiate, Vector2Int cellPosition, Quaternion quaternion)
//         {
//             throw new System.NotImplementedException();
//         }
//
//         public override void Replace(GameObject objectToReplace, Vector2Int newCellPosition)
//         {
//             objectToReplace.transform.position = GetPosition(newCellPosition);
//             TrySetOrderInLayer(objectToReplace, GetAdditionalSortingOrder(newCellPosition)); 
//         }
//         
//         public override Vector2 GetPosition(Vector2Int cell)
//         {
//             Vector2 position =(Vector2)cell;
//             position = new Vector2
//                 (position.x -cell.x/pixelsPerUnit, position.y -cell.y/pixelsPerUnit);
//             return MazeData.CornerPosition + position;
//         }
//
//         private int GetAdditionalSortingOrder(Vector2Int cellPosition)
//         {
//             return _grid.Length - cellPosition.y + 1; 
//         }
//     }
// }