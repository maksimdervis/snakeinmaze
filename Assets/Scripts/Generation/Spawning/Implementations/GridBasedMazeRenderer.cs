using System;
using Generation.Cells;
using Generation.Cells.Abstractions;
using Generation.Cells.Implementations;
using Generation.Spawning.Abstractions;
using Unity.VisualScripting;
using UnityEngine;
using Zenject;
using Grid = Generation.MazeHolding.Abstractions.Grid;

namespace Generation.Spawning.Implementations
{
    public class GridBasedMazeRenderer : MazeRenderer
    {
        [SerializeField] private Cell food;
        private GridPositioner _gridPositioner;
        private Exit exit;

        [Inject]
        public void Construct(GridPositioner gameObjectsGenerative, Grid grid)
        {
            base.Construct(grid);
            LeftFoodNumber = grid.LeftFoodNumber;
            _gridPositioner = gameObjectsGenerative;
        }

        public override void InstantiateMaze()
        {
            for (int x = 0; x < Grid.Length; x++)
            {
                for (int y = 0; y < Grid.Length; y++)
                {
                    Vector2Int position = new Vector2Int(x, y);
                    Cell newCell = Grid.GetCellAt(position);
                    Cell instantiatedCell = _gridPositioner.Instantiate(newCell, position);
                    Cells[x, y].Add(instantiatedCell);
                    if (Grid.ContainsFoodAt(position))
                        Cells[x, y].Add(_gridPositioner.Instantiate(food, position));
                    if (newCell.GetType() == typeof(Exit))
                        exit = (Exit) instantiatedCell;
                }
            }
        }

        public override bool TryExit() => 
            exit.TryGoThrough();

        public override void EatFoodAt(Vector2Int at)
        {
            Food foundFood = (Food) Cells[at.x, at.y].Find((cell) => cell.GetType() == typeof(Food));
            foundFood.MakeEaten();
            Grid.EatFoodAt(at);
            LeftFoodNumber--;
            if (LeftFoodNumber == 0)
                exit.Open();
        }
    }
}