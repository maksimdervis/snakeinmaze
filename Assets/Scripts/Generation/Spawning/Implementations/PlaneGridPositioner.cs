﻿using Generation.Cells.Abstractions;
using Generation.Spawning.Abstractions;
using Unity.Mathematics;
using UnityEngine;

namespace Generation.Spawning.Implementations
{
    public class PlaneGridPositioner : GridPositioner
    {
        public override TCell Instantiate<TCell>(TCell objectToInstantiate, Vector2Int cellPosition) => 
            (TCell) CellFactory.Create(objectToInstantiate, cellPosition);

        public override void Replace<TCell>(TCell objectToReplace, Vector2Int newCellPosition) => 
            objectToReplace.gameObject.transform.position = (Vector2) newCellPosition;

        public override Vector2 GetPosition(Vector2Int cell) => cell;
    }
}