﻿using Debug;
using GamePlay;
using GamePlay.Player.Implementations;
using Infrastructure.Services.Input;
using SnakeCamera;
using UnityEngine;
using Zenject;

namespace Infrastructure.DI
{
    public class GamePlayServicesInstaller : MonoInstaller
    {
        [SerializeField] private Snake snake; 
        [SerializeField] private Camera cam;
        [SerializeField] private KeyboardInputService keyboardInputService;
        [SerializeField] private SwipeInputService swipeInputService;
        [SerializeField] private JoystickInputService joystickInputService;
        [SerializeField] private InputService standaloneInputService;

        public override void InstallBindings()
        {
            InstallInstances();
            InstallInputServices();
            InstallSettings();
        }

        private void InstallSettings()
        {
            Container.Bind<InputSettings>().ToSelf().AsSingle();
            Container.Bind<SnakeSettings>().ToSelf().AsSingle();
            Container.Bind<DebugSettings>().ToSelf().AsSingle();
            Container.Bind<CameraConfiguration>().ToSelf().AsSingle();
        }

        private void InstallInstances()
        {
            Container.Bind<Snake>().FromInstance(snake).AsSingle();
            Container.Bind<Camera>().FromInstance(cam).AsSingle();
        }
        
        private void InstallInputServices()
        {
            Container.Bind<KeyboardInputService>().FromInstance(keyboardInputService);
            Container.Bind<SwipeInputService>().FromInstance(swipeInputService);
            Container.Bind<JoystickInputService>().FromInstance(joystickInputService);
            Container.Bind<InputService>().FromInstance(standaloneInputService);
        }
    }
}