using Generation.MazeHolding.Abstractions;
using Generation.MazeHolding.Implementations;
using Generation.Spawning.Abstractions;
using Generation.Spawning.Implementations;
using SnakeCamera;
using UnityEngine;
using Zenject;
using CellFactory = Generation.Cells.Abstractions.Cell.CellFactory;
using Grid = Generation.MazeHolding.Abstractions.Grid;
using GridPositioner = Generation.Spawning.Abstractions.GridPositioner;

namespace Infrastructure.DI
{
    public class MazeInstaller : MonoInstaller
    {
        [SerializeField] private GridBasedMazeRenderer gridBasedMazeRenderer;
        [SerializeField] private PlaneGridPositioner planeGridPositioner;
        [SerializeField] private RandomMaze randomMaze;
        [SerializeField] private CameraScale cameraScale;
        
        public override void InstallBindings()
        {
            Container.Bind<CellFactory>().AsSingle();
            Container.Bind<IMazeData>().To<LevelBasedMazeData>().AsSingle();
            Container.Bind<CustomRandom<int>>().To<LevelBasedRandomizer>().AsSingle();
            Container.Bind<Grid>().FromInstance(randomMaze).AsSingle();
            Container.Bind<GridPositioner>().FromInstance(planeGridPositioner).AsSingle();
            Container.Bind<MazeRenderer>().FromInstance(gridBasedMazeRenderer).AsSingle();
            Container.Bind<CameraScale>().FromInstance(cameraScale);
        }   
    }
}
