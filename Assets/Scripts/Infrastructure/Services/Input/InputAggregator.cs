﻿using System;
using System.Collections.Generic;
using Enums;

namespace Infrastructure.Services.Input
{
    public abstract class InputAggregator : InputService
    {
        private readonly List<InputService> _inputSystems = new();

        public override void OnGotMoveDirection(Action<Direction> action)
        {
            foreach (var inputSystem in _inputSystems)
                inputSystem.OnGotMoveDirection(action);
        }

        protected void AddInputSystems(params InputService[] inputSystems)
        {
            foreach(InputService inputSystem in inputSystems)
                _inputSystems.Add(inputSystem);
        }
    }
}