﻿using Zenject;

namespace Infrastructure.Services.Input
{
    public class StandaloneInputAggregator : InputAggregator
    {
        [Inject]
        public void Construct(KeyboardInputService keyboardInputService, SwipeInputService swipeInputService, JoystickInputService joystickInputService) => 
            AddInputSystems(swipeInputService, keyboardInputService, joystickInputService);
    }
}