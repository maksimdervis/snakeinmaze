using System;
using UnityEngine;

namespace Infrastructure.Services.Input
{
    public class KeyboardInputService : InputService
    {
        protected override void ProcessInput()
        {
            float horizontalAxis = SimpleInput.GetAxis("Horizontal");
            float verticalAxis = SimpleInput.GetAxis("Vertical");
            ProcessGotAxis(horizontalAxis, verticalAxis);
        }
    }
}
