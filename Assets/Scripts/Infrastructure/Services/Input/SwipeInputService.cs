﻿using UnityEngine.EventSystems;

namespace Infrastructure.Services.Input
{
    public class SwipeInputService : InputService, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public void OnBeginDrag(PointerEventData eventData) => 
            ProcessGotAxis(eventData.delta.x, eventData.delta.y);

        public void OnDrag(PointerEventData eventData)
        {
        }
        
        
        public void OnEndDrag(PointerEventData eventData)
        {
        }
    }
}