﻿using System;
using Enums;
using UnityEngine;

namespace Infrastructure.Services.Input
{
    public abstract class InputService : MonoBehaviour
    {
        private Action<Direction> _onGotMoveDirection;

        public virtual void OnGotMoveDirection(Action<Direction> action)
        {
            _onGotMoveDirection += action;
        }

        public void Switch() => 
            gameObject.SetActive(gameObject.activeSelf == false);

        protected void ProcessGotAxis(float horizontal, float vertical)
        {
            if (Math.Abs(horizontal) > Math.Abs(vertical))
                ProcessHorizontalAxis(horizontal);
            else
                ProcessVerticalAxis(vertical);
        }

        private void Invoke(Direction direction)
        {
            _onGotMoveDirection?.Invoke(direction);
        }

        private void ProcessVerticalAxis(float vertical)
        {
            switch (vertical)
            {
                case > 0:
                    Invoke(Direction.Down);
                    return;
                case < 0:
                    Invoke(Direction.Up);
                    return;
            }
        }

        private void ProcessHorizontalAxis(float horizontal)
        {
            switch (horizontal)
            {
                case > 0:
                    Invoke(Direction.Right);
                    return;
                case < 0:
                    Invoke(Direction.Left);
                    return;
            }
        }

        private void Update()
        {
            if (Time.timeSinceLevelLoad < 0.5f)
                return;
            ProcessInput();
        }

        protected virtual void ProcessInput() {} 
    }
}