﻿using SimpleInputNamespace;
using UnityEngine;

namespace Infrastructure.Services.Input
{
    public class JoystickInputService : InputService
    {
        [SerializeField] private Joystick joystick;

        public float JoystickMovementAreaRadius
            { set => joystick.MovementAreaRadius = value; }
        
        protected override void ProcessInput()
        {
            float horizontalAxis = SimpleInput.GetAxis("Horizontal");
            float verticalAxis = SimpleInput.GetAxis("Vertical");
            ProcessGotAxis(horizontalAxis, verticalAxis);
        }
    }
}