﻿using System;
using UnityEngine;
using Zenject;

namespace Infrastructure.Services.Input
{
    public class InputSettings
    {
        public float JoystickMovementAreaRadius
            { set => _joystickInputService.JoystickMovementAreaRadius = value; }
        
        private readonly SwipeInputService _swipeInputService;
        private readonly JoystickInputService _joystickInputService;
        
        public InputSettings(SwipeInputService swipeInputService, JoystickInputService joystickInputService)
        {
            _swipeInputService = swipeInputService;
            _joystickInputService = joystickInputService;
            SwitchOnJoystickInputSystem();
        }

        public void SwitchOnSwipeInputSystem()
        {
            _swipeInputService.gameObject.SetActive(true);
            _joystickInputService.gameObject.SetActive(false);
        }

        public void SwitchOnJoystickInputSystem()
        {
            _joystickInputService.gameObject.SetActive(true);
            _swipeInputService.gameObject.SetActive(false);
        }
    }
}