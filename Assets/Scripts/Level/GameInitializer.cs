﻿using System;
using Generation.Spawning.Abstractions;
using UnityEngine;
using Zenject;

namespace Level
{
    public class GameInitializer : MonoBehaviour
    {
        private MazeRenderer _mazeRenderer;

        [Inject]
        public void Construct(MazeRenderer mazeRenderer)
        {
            _mazeRenderer = mazeRenderer;
            _mazeRenderer.InstantiateMaze();
        }
    }
}