using System;
using System.ComponentModel;
using Constants;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Zenject;

namespace Level
{
    public class LevelChoose : MonoBehaviour
    {
        [SerializeField] private InputField levelInputField;
        
        private ZenjectSceneLoader _zenjectSceneLoader;

        [Inject]
        public void Construct(ZenjectSceneLoader zenjectSceneLoader)
        {
            _zenjectSceneLoader = zenjectSceneLoader;
        }

        public void OnPlayButtonClicked()
        {
            if (!Int32.TryParse(levelInputField.text, out int level))
                return;
            
            _zenjectSceneLoader.LoadScene("Game", LoadSceneMode.Single, (container) => 
                { container.BindInstance(level).WithId("Level"); });
        }
        
        
        
        
    }
}
