﻿namespace Constants
{
    public static class MazeConstants
    {
        public const int DefaultSector = 3;
        public const int MinSector = 2;
        public const int MaxSector = 4;
        public const int CellLength = 1;
        public const int AddLevel = 37;
        public const int StartSpeedLevel = 100;

        public static int GetLength(int sectors) =>
            (DefaultSector + CellLength) * sectors + CellLength;

        public static int GetSectorFirstCell(int coordinate) => 
            coordinate * (DefaultSector + 1) + 1;
    }
}