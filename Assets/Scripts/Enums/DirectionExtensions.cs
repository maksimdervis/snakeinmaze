﻿using System;
using UnityEngine;

namespace Enums
{
    public static class DirectionExtensions
    {
        public static Vector2Int ToVector2Int(this Direction direction) =>
            direction switch
            {
                Direction.Down => Vector2Int.up,
                Direction.Up => Vector2Int.down,
                Direction.Right => Vector2Int.right,
                Direction.Left => Vector2Int.left,
                _ => throw new ArgumentOutOfRangeException(nameof(direction), direction, "Got invalid direction value")
            };

        public static Direction GetOpposite(this Direction direction)
        {
            Vector2Int vectorDirection = direction.ToVector2Int();
            Vector2Int oppositeVectorDirection = -vectorDirection;
            return GetDirection(oppositeVectorDirection);
        }

        public static Direction GetDirection(Vector2Int direction)
        {
            if (direction == Vector2Int.up)
                return Direction.Down;
            if (direction == Vector2Int.down)
                return Direction.Up;
            if (direction == Vector2Int.right)
                return Direction.Right;
            if (direction == Vector2Int.left)
                return Direction.Left; 
            throw new ArgumentOutOfRangeException(nameof(direction), direction, "Got invalid direction value");
        }
    }
}