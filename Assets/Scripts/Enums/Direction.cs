﻿namespace Enums
{
    public enum Direction
    {
        Right, 
        Left, 
        Up,
        Down
    }
}