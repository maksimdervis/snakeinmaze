﻿using System;
using Constants;
using Generation.MazeHolding.Abstractions;

namespace GamePlay
{
    public class SnakeSettings
    {
        private Action onSnakeAnimationChanged; 
        
        private float _timeBeforeMiddleFrame;
        private float _timeAfterMiddleFrame;
        private float _framesRatio;

        public SnakeSettings(IMazeData mazeData)
        {
            if (TempSnakeSettings.Pass)
            {
                _framesRatio = TempSnakeSettings.ratio;
                TimeBeforeMiddleFrame = 1 / TempSnakeSettings.SnakeSpeed;
                return;
            }
            
            _framesRatio = 0.1f + (mazeData.Level + MazeConstants.StartSpeedLevel - 1) * (0.3f - 0.1f) / 500;
            TimeBeforeMiddleFrame = 1 / (3f + (5f - 3f) / (500 - MazeConstants.AddLevel));
            IsGod = true;
        }

        public void OnSnakeAnimationChanged(Action action) => 
            onSnakeAnimationChanged += action;

        public float TimeBeforeMiddleFrame
        {
            get => _timeBeforeMiddleFrame;
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException(nameof(value), 
                        "Time can't be less of equal to zero");
                _timeBeforeMiddleFrame = value;
                _timeAfterMiddleFrame = value * _framesRatio; 
                onSnakeAnimationChanged?.Invoke();
            }
        }

        public float TimeAfterMiddleFrame => _timeAfterMiddleFrame;

        public float FramesRatio
        {
            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException(nameof(value), 
                        "Frames ratio can't be less or equal to zero");
                _framesRatio = value;
                TimeBeforeMiddleFrame = _timeBeforeMiddleFrame; 
                onSnakeAnimationChanged?.Invoke();
            }
        }

        public bool IsGod { get; set; }
    }
}