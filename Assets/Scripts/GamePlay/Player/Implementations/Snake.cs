﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Enums;
using Generation.Cells.Implementations;
using Generation.Spawning.Abstractions;
using UnityEngine;
using UnityEngine.Events;
using Zenject;
using Grid = Generation.MazeHolding.Abstractions.Grid;

namespace GamePlay.Player.Implementations
{
    public class Snake : MonoBehaviour
    {
        [SerializeField] private float maxDestructionTime = 2f;
        [SerializeField] private SnakePart snakePart;
        [SerializeField] private InnerSnake innerSnakePart;

        public Vector2Int CurrentHeadCell { get; private set; }
        
        private SnakeSettings _snakeSettings;

        private GridPositioner _gridPositioner;
        private Grid _grid;
        private List<SnakePart> _bodyParts;
        private Coroutine _moveCoroutine;
        private UnityEvent _onSnakeMoved;
        private MazeRenderer _mazeRenderer;
        private Direction _savedHeadDirection;
        private Direction _currentHeadDirection;
        private bool _isSecondTickPeriod;
        private bool _isGameOver;
        private bool _isMovingForward; 
        private bool _isMovementStarted;
        private int timesSnakeTurned; 
        private WaitForSeconds _waitAfterMiddleFrame;
        private WaitForSeconds _waitBeforeMiddleFrame;
        private WaitForSeconds _waitBeforePartDestruction;
        private ZenjectSceneLoader _sceneLoader;
        private int _foodBeforeElongation;
        private InnerSnake _innerSnake;
        private bool _reverseIndexes;
        private int _currentLength;
        private int _headInBodyIndex;

        [Inject]
        public void Construct(ZenjectSceneLoader sceneLoader, GridPositioner gridPositioner, MazeRenderer mazeRenderer, Grid grid, SnakeSettings snakeSettings)
        {
            _sceneLoader = sceneLoader;
            _gridPositioner = gridPositioner;
            _grid = grid;
            _mazeRenderer = mazeRenderer;
            _snakeSettings = snakeSettings; 
            _snakeSettings.OnSnakeAnimationChanged(UpdateSnakeMoveAnimation);
            UpdateSnakeMoveAnimation();
            CurrentHeadCell = _grid.StartCell;
            _onSnakeMoved = new UnityEvent();
            _isMovementStarted = false;
            _isMovingForward = true;
            _foodBeforeElongation = 1;
            _currentLength = 1; 
        }

        private void UpdateSnakeMoveAnimation()
        {
            _waitBeforeMiddleFrame = new WaitForSeconds(_snakeSettings.TimeBeforeMiddleFrame);
            _waitAfterMiddleFrame = new WaitForSeconds(_snakeSettings.TimeAfterMiddleFrame);
        }

        private IEnumerator Move()
        {
            _isGameOver = false;
            while (_isGameOver == false)
            {
                if (_isMovingForward)
                {
                    _isSecondTickPeriod = false;
                    _isGameOver = DoFirstHalfOfMovement() == false;
                    yield return _waitAfterMiddleFrame;
                    DoSecondHalfOfMovement();
                    _onSnakeMoved.Invoke();
                    _isSecondTickPeriod = true;
                    yield return _waitBeforeMiddleFrame;
                }
                else
                {
                    Vector2 currentPosition = _gridPositioner.GetPosition(CurrentHeadCell);
                    Vector2Int previousHeadCell = CurrentHeadCell; 
                    UnityEngine.Debug.Log(_headInBodyIndex);
                    if(_bodyParts.Any((part) => part.CellPosition == CurrentHeadCell + _savedHeadDirection.ToVector2Int()) == false && 
                    _headInBodyIndex != 0 && _headInBodyIndex != _currentLength - 1)
                        _savedHeadDirection = DirectionExtensions.GetDirection(_bodyParts[_headInBodyIndex + 1].CellPosition - CurrentHeadCell);

                    CurrentHeadCell += _savedHeadDirection.ToVector2Int();
                    _currentHeadDirection = _savedHeadDirection;

                    if (CurrentHeadCell == _bodyParts[^1].CellPosition)
                    {
                        _bodyParts.Reverse();
                        foreach (SnakePart part in _bodyParts)
                            (part.Next, part.Previous) = (part.Previous, part.Next);
                        
                        _isMovingForward = true;
                        timesSnakeTurned = 0;
                    }
                    else if (_bodyParts.Any((part) => part.CellPosition == CurrentHeadCell) == false)
                    {
                        if (previousHeadCell == _bodyParts[0].CellPosition)
                        {
                            CurrentHeadCell = previousHeadCell;
                            _isMovingForward = true;
                            timesSnakeTurned = 0;
                            continue;
                        }
                        
                        StopGame();
                    }

                    _headInBodyIndex += (_reverseIndexes ? -1 : 1); 
                    Vector2 nextPosition = _gridPositioner.GetPosition(CurrentHeadCell);
                    Vector2 middlePosition = (currentPosition + nextPosition) / 2;
                    _isSecondTickPeriod = false;
                    _innerSnake.transform.position = middlePosition;
                    yield return _waitAfterMiddleFrame;
                    _isSecondTickPeriod = true;
                    _innerSnake.transform.position = nextPosition;
                    _onSnakeMoved.Invoke();
                    yield return _waitBeforeMiddleFrame;
                }
            }
        }

        private void DoSecondHalfOfMovement()
        {
            if (TryLengthenSnake() == false)
                RemoveTail();
            _gridPositioner.Replace(_innerSnake, CurrentHeadCell);
            PutCorrectSprites();
            SetEndSprites();
        }

        private bool DoFirstHalfOfMovement()
        {
            if (TryMove(_savedHeadDirection) == false)
                return false;
            
            SetOrdersInLayer();
            PutCorrectSprites();
            if (_grid.ContainsFoodAt(CurrentHeadCell))
                _bodyParts[0].SetHalfEndSprite();
            else
                SetHalfEndSprites();
            return true;
        }

        private void RemoveTail()
        {
            Destroy(_bodyParts[^1].gameObject);
            _bodyParts.RemoveAt(_bodyParts.Count - 1);
            _bodyParts[^1].Previous = null;
        }

        private void SetEndSprites()
        {
            _bodyParts[0].SetEndSprite();
            _bodyParts[^1].SetEndSprite();
        }

        private void SetHalfEndSprites()
        {
            _bodyParts[0].SetHalfEndSprite();
            _bodyParts[^1].SetHalfEndSprite();
        }

        public void OnSnakeMoved(UnityAction action) => 
            _onSnakeMoved.AddListener(action);

        public void UnsubscribeAll() => 
            _onSnakeMoved.RemoveAllListeners();

        private bool TryMove(Direction direction)
        {
            _currentHeadDirection = direction;
            CurrentHeadCell += direction.ToVector2Int();
            if (IsKillingHeadPosition() && _snakeSettings.IsGod == false)
            {
                StopGame();
                return false;
            }

            if (IsExitHeadPosition())
            {
                if (_mazeRenderer.TryExit())
                {
                    if (_moveCoroutine != null)
                        StopCoroutine(_moveCoroutine);
                    _isGameOver = true;
                    return false;
                }
            }
            
            SnakePart newHead = InstantiateNewBodyPart(CurrentHeadCell);
            _innerSnake.transform.position += (Vector3) (Vector2) direction.ToVector2Int() / 2f;
            newHead.Previous = _bodyParts[0];
            _bodyParts[0].Next = newHead;
            _bodyParts.Insert(0, newHead);
            return true;
        }

        private void StopGame()
        {
            if (_moveCoroutine != null)
                StopCoroutine(_moveCoroutine);
            _isGameOver = true;
            StartCoroutine(DestroySnake());
        }

        private bool IsKillingHeadPosition() =>
            _grid.GetCellAt(CurrentHeadCell).GetType() == typeof(Wall) ||
            _bodyParts.Exists(part => part.CellPosition == CurrentHeadCell);

        private bool IsExitHeadPosition() => 
            _grid.GetCellAt(CurrentHeadCell).GetType() == typeof(Exit);

        private IEnumerator DestroySnake()
        {
            float timeGone = 0f;
            foreach (SnakePart part in _bodyParts)
            {
                Destroy(part.gameObject);
                if (timeGone > maxDestructionTime)
                {
                    _sceneLoader.LoadScene("Initial");
                    yield break;
                }
                
                yield return _waitBeforePartDestruction; 
            }

            _sceneLoader.LoadScene("Initial");
        }
        
        private bool TryLengthenSnake()
        {
            if (_grid.ContainsFoodAt(CurrentHeadCell) == false) 
                return false;
            
            _mazeRenderer.EatFoodAt(CurrentHeadCell);
            if (_foodBeforeElongation-- > 1)
                return false;

            _currentLength++;
            _foodBeforeElongation = _bodyParts.Count;
            return true;
        }

        private void SetOrdersInLayer()
        {
            SetEqualOrdersInLayer();
            _bodyParts[0].OrderInLayer += _bodyParts.Count;
            for (int i = 1; i < _bodyParts.Count; i++) 
                _bodyParts[i].OrderInLayer = _bodyParts[i - 1].OrderInLayer - 1;
        }

        private void SetEqualOrdersInLayer()
        {
            foreach (SnakePart current in _bodyParts)
                current.OrderInLayer = _bodyParts[^1].OrderInLayer;
        }

        private void PutCorrectSprites()
        {
            foreach (SnakePart current in _bodyParts)
                current.PutCorrectSprite();
        }

        private void Start()
        {
            _isSecondTickPeriod = true;
            _bodyParts = new List<SnakePart>();

            AddBodyPart(CurrentHeadCell);
            _innerSnake = _gridPositioner.Instantiate(innerSnakePart, CurrentHeadCell);
            PutCorrectSprites();
            SetOrdersInLayer();
        }

        public void TryTurn(Direction direction)
        {
            if (_isMovingForward == false)
            {
                if (_savedHeadDirection == direction.GetOpposite())
                {
                    if (timesSnakeTurned >= 2)
                    {
                        _headInBodyIndex = 0;
                        _reverseIndexes = false;
                        return;
                    }

                    _reverseIndexes = true;
                    timesSnakeTurned++;
                }

                _savedHeadDirection = direction;
                return;
            }
            
            if (IsPossibleToTurn(direction) == false)
            {
                if (_currentHeadDirection == direction.GetOpposite() && _currentLength != 1)
                {
                    timesSnakeTurned++;
                    _headInBodyIndex = 0;
                    _reverseIndexes = false;
                    _isMovingForward = false;
                    _savedHeadDirection = direction;
                }

                return;
            }

            _savedHeadDirection = direction;
            TryTurnInstantly();
        }
        
        private bool IsPossibleToTurn(Direction direction)
        {
            if (_isMovementStarted == false)
                return true; 
            
            if (_currentHeadDirection == direction.GetOpposite())
                return false;
            
            if (_currentHeadDirection == direction && _isMovementStarted)
                return false;
            
            return !_isGameOver;
        }

        private bool TryTurnInstantly()
        {
            if (_isSecondTickPeriod)
            {
                RestartMovement();
                return true;
            }

            return false;
        }

        private void RestartMovement()
        {
            _isMovementStarted = true; 
            if (_moveCoroutine != null)
                StopCoroutine(_moveCoroutine);
            _moveCoroutine = StartCoroutine(Move());
        }

        private SnakePart InstantiateNewBodyPart(Vector2Int cellPosition)
        {
            SnakePart newBodyPart = _gridPositioner.Instantiate(snakePart, cellPosition);
            newBodyPart.CellPosition = cellPosition;
            return newBodyPart;
        }

        private SnakePart AddBodyPart(Vector2Int cellPosition)
        {
            SnakePart newBodyPart = InstantiateNewBodyPart(cellPosition);
            _bodyParts.Add(newBodyPart);
            if (_bodyParts.Count != 1)
            {
                _bodyParts[^1].Next = _bodyParts[^2];
                _bodyParts[^2].Previous = newBodyPart;
            }
            SetOrdersInLayer();
            return newBodyPart;
        }
    }
}