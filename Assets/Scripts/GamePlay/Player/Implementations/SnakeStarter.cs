﻿using Generation.Spawning.Abstractions;
using Infrastructure.Services;
using Infrastructure.Services.Input;
using UnityEngine;
using Zenject;
using Quaternion = UnityEngine.Quaternion;

namespace GamePlay.Player.Implementations
{
    public class SnakeStarter : MonoBehaviour
    {
        private Snake _snake;
        private InputService _inputService;

        [Inject]
        public void Construct(InputService inputService, Snake snake)
        {
            _inputService = inputService;
            _snake = snake;

            _inputService.OnGotMoveDirection(_snake.TryTurn);
        }
    }
}