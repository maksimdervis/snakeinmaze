﻿using System;
using Generation.Cells;
using Generation.Cells.Abstractions;
using JetBrains.Annotations;
using ModestTree;
using Unity.VisualScripting;
using UnityEngine;

namespace GamePlay.Player.Implementations
{
    public class SnakePart : Cell
    {
        public Vector2Int CellPosition;// { get; set; }
        [CanBeNull] public SnakePart Next;// { get; set; }
        [CanBeNull] public SnakePart Previous;// { get; set; }

        public int OrderInLayer
        {
            get => spriteRenderer.sortingOrder;
            set => spriteRenderer.sortingOrder = value;
        }

        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Sprite startSnake;
        [SerializeField] private Sprite angle;
        [SerializeField] private Sprite verticalEnd;
        [SerializeField] private Sprite horizontalEnd;
        [SerializeField] private Sprite startVerticalHalfEnd;
        [SerializeField] private Sprite startHorizontalHalfEnd;
        [SerializeField] private Sprite tailVerticalHalfEnd;
        [SerializeField] private Sprite tailHorizontalHalfEnd;
        [SerializeField] private Sprite verticalBody;
        [SerializeField] private Sprite horizontalBody;

        public void PutCorrectSprite()
        {
            if (TryPutStartSnakeSprite(startSnake))
                return;

            if (TryPutEndSprite(horizontalEnd, verticalEnd))
                return;

            if (TryPutLinearBodySprite())
                return;

            TryPutAngleSprite();
        }

        private bool TryPutAngleSprite()
        {
            if (Previous.CellPosition.x == Next.CellPosition.x ||
                Previous.CellPosition.y == Next.CellPosition.y)
                return false;
            spriteRenderer.sprite = angle;
            spriteRenderer.flipX = CellPosition.x > Previous.CellPosition.x ||
                                   CellPosition.x > Next.CellPosition.x;
            spriteRenderer.flipY = CellPosition.y < Previous.CellPosition.y ||
                                   CellPosition.y < Next.CellPosition.y;
            return true;
        }

        private bool TryPutLinearBodySprite()
        {
            if (Previous.CellPosition.y == Next.CellPosition.y)
            {
                spriteRenderer.sprite = horizontalBody;
                return true;
            }

            if (Previous.CellPosition.x == Next.CellPosition.x)
            {
                spriteRenderer.sprite = verticalBody;
                return true;
            }

            return false;
        }

        private bool TryPutStartSnakeSprite(Sprite startSprite)
        {
            if (Next == null && Previous == null)
            {
                spriteRenderer.sprite = startSprite;
                return true;
            }

            return false;
        }

        private bool TryPutEndSprite(Sprite horizontal, Sprite vertical)
        {
            return TryPutEndSprite(horizontal, vertical, horizontal, vertical);
        }

        private bool TryPutEndSprite(Sprite horizontal, Sprite vertical, Sprite startHorizontal, Sprite startVertical)
        {
            if (Next == null)
            {
                if (Previous.Previous == null)
                    return TryPutCorrectEndSprite(Previous, startHorizontal, startVertical);
                
                if (TryPutCorrectEndSprite(Previous, horizontal, vertical))
                    return true;
            }

            if (Previous == null)
            {
                if (Next.Next == null)
                    return TryPutCorrectEndSprite(Next, startHorizontal, startVertical); 
                
                if (TryPutCorrectEndSprite(Next, horizontal, vertical))
                    return true;
            }

            return false;
        }

        private bool TryPutCorrectEndSprite(SnakePart neighbour, Sprite horizontal, Sprite vertical)
        {
            if (TryPutVerticalEndSprite(neighbour, vertical)) 
                return true;
            if (TryPutHorizontalEndSprite(neighbour, horizontal)) 
                return true;
            return false;
        }

        private bool TryPutHorizontalEndSprite(SnakePart neighbour, Sprite horizontal)
        {
            if (CellPosition.y == neighbour.CellPosition.y)
            {
                spriteRenderer.sprite = horizontal;
                spriteRenderer.flipX = CellPosition.x < neighbour.CellPosition.x;
                return true;
            }

            return false;
        }

        private bool TryPutVerticalEndSprite(SnakePart neighbour, Sprite vertical)
        {
            if (CellPosition.x == neighbour.CellPosition.x)
            {
                spriteRenderer.sprite = vertical;
                spriteRenderer.flipY = CellPosition.y > neighbour.CellPosition.y;
                return true;
            }

            return false;
        }

        public void SetHalfEndSprite()
        {
            if (TryPutEndSprite(tailHorizontalHalfEnd, tailVerticalHalfEnd, startHorizontalHalfEnd, startVerticalHalfEnd)) 
                return;

            throw new NotImplementedException("Snake part must be snake or tail to use this method");
        }
        
        public void SetEndSprite()
        {
            if (TryPutStartSnakeSprite(startSnake))
                return;
            
            if (TryPutEndSprite(horizontalEnd, verticalEnd)) 
                return;

            throw new NotImplementedException("Snake part must be snake or tail to use this method");
        }
    }
}