﻿using System.Collections.Generic;
using Generation.MazeHolding.Abstractions;

namespace Extensions
{
    public static class ListExtensions
    {
        public static void Shuffle<T>(this IList<T> list, CustomRandom<int> random)
        {
            int left = list.Count;
            while (left > 1)
            {
                int k = random.GetRandom(0, left);
                (list[k], list[left - 1]) = (list[left - 1], list[k]);
                left--;
            }
        }
    }
}